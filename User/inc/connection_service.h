
#ifndef _CONNETCTION_SERVICE_H_
#define _CONNETCTION_SERVICE_H_
#include <stdint.h>


#define ADVERTLEN 2
#define ADVERTTYPE 1
#define ADVERTUUIDLEN 17
#define ADVERTUUIDTYPE 7
#define GAP_ADTYPE_LOCAL_NAME_COMPLETE 9
// EcomoTap GATT Profile Service UUID: "b8510e22-7500-4608-bafe-6b72c2531e14"
#define	ECOMO_TAP_SERVICE_UUID_ARRAY		0x14, 0x1e, 0x53, 0xc2, 0x72, 0x6b, 0xfe, 0xba, 0x08, 0x46, 0x00, 0x75, 0x22, 0x0e, 0x51, 0xb8
#define GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE    0x12
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     80
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     800


#define GAP_ADTYPE_POWER_LEVEL                  0x0A
#define MAX_NUM_BLE_CONNS 						2


typedef struct
{
	uint8_t			gatt_read_flag;
	uint8_t			is_Authenticated;   
} auth_Struct;
typedef struct
{
	uint8_t			connet_flag[2];
    uint16_t		connet_handle[2];
    
} index_handle;

extern index_handle g_index_handle;
extern uint16_t g_connecthandle;
extern auth_Struct g_auth_state [MAX_NUM_BLE_CONNS];
uint8_t fount_device_init(void);
void user_task(void);
void Process_InputData(uint8_t* data_buffer, uint16_t Nb_bytes);
uint8_t creat_connection(void);
uint8_t  get_index(uint16_t Connection_Handle);
#endif 
