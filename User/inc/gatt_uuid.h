

#ifndef _GATT_UUID_H_
#define _GATT_UUID_H_
#include <stdint.h>

// Device Information Service Parameters
#define STATUS_SUCCESS										0
#define STATUS_ERR										    0XFF
#define DEVINFO_SYSTEM_ID                 0
#define DEVINFO_MODEL_NUMBER              1
#define DEVINFO_SERIAL_NUMBER             2
#define DEVINFO_FIRMWARE_REV              3
#define DEVINFO_HARDWARE_REV              4
#define DEVINFO_SOFTWARE_REV              5
#define DEVINFO_MANUFACTURER_NAME         6
#define DEVINFO_11073_CERT_DATA           7
#define DEVINFO_PNP_ID                    8
#define DEVINFO_SYSTEM_ID_LEN							8
#define DEVINFO_STR_ATTR_LEN              20
#define	CHARACTERISTIC_MAX_BYTES	20
#define	ECOMO_VERSION_MAX_BYTES		CHARACTERISTIC_MAX_BYTES
#define BLEINVALIDRANGE                 0x18  //!< A parameter is out of range
typedef struct 
{
		uint16_t 	devinfoservehandle;
		uint16_t 	appservehandle;
		uint16_t	apptxhandle;
		uint16_t	apprxhandle;
		uint16_t	appindicatehandle;
		uint16_t 	idSystemHandle;
		uint16_t 	modelNumbHandle;
		uint16_t 	serialNumberHandle;
		uint16_t 	firmwareRevHandle;
		uint16_t 	hardwareRevHandle;
		uint16_t 	softWareRevHandle;
	 	uint16_t 	devInfoMfrNamePropsHandle;
	    uint16_t  ieee11073Data;
		uint16_t 	pnpIDHandle;
}Bt_handle_t;

typedef uint8_t tBleStatus;
tBleStatus add_dev_info_att_service(void);
uint8_t set_dev_info_parmeter(uint8_t decinfoname,uint8_t len,uint8_t *value);
Bt_handle_t get_att_handle(void);
uint8_t  ecomo_set_version (uint8_t len, uint8_t *pData);
uint8_t  gatt_update_notify_value (uint8_t channel, uint16_t connHandle, uint8_t len, uint8_t *pData);
#endif /* _GATT_DB_H_ */
