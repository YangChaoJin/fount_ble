/******************** (C) COPYRIGHT 2015 STMicroelectronics ********************
* File Name          : BLE_Chat_main.c
* Author             : AMS - VMA RF Application Team
* Version            : V1.1.0
* Date               : 15-January-2016
* Description        : BlueNRG-1 main file for Chat demo
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/
/** @addtogroup BlueNRG1_demonstrations_applications
 * BlueNRG-1 Chat demo \see BLE_Chat_main.c for documentation.
 *
 *@{
 */

/** @} */
/** \cond DOXYGEN_SHOULD_SKIP_THIS
 */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "BlueNRG1_it.h"
#include "BlueNRG1_conf.h"
#include "ble_const.h" 
#include "bluenrg1_stack.h"
#include "gp_timer.h"
#include "app_state.h"
#include "connection_service.h"
#include "SDK_EVAL_Config.h"
#include "Fount_config.h"
#include "OTA_btl.h"
#include "gp_timer.h"
#include "apps_program.h"
#include "ble_mutually.h"
#include "MTI.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define BLE_CHAT_VERSION_STRING "1.0.0" 


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

int main(void) 
{
    uint8_t ret;
    /* System Init */
    SystemInit();

    /* Identify BlueNRG1 platform */
    SdkEvalIdentification();

    /* Init Clock */
    Clock_Init();

    SdkEvalComUartInit(UART_BAUDRATE);
    SdkEvalComUartIrqConfig(ENABLE);
    /* BlueNRG-1 stack init */
    ret = BlueNRG_Stack_Initialization(&BlueNRG_Stack_Init_params);
    if (ret != BLE_STATUS_SUCCESS)
    {
        printf("Error in BlueNRG_Stack_Initialization() 0x%02x\r\n", ret);
        while(1);
    }

    printf("BlueNRG-1 BLE Chat Server Application (version: %s)\r\n", BLE_CHAT_VERSION_STRING);

    /*  初始化与MCU的传输管理          */
    mti_init ();

    /* Init Chat Device */
    ret = fount_device_init();
    if (ret != BLE_STATUS_SUCCESS)
    {
        printf("fount_device_init()--> Failed 0x%02x\r\n", ret);
        while(1);
    }
    printf("BLE Stack Initialized \n");
    

    //queue_init();
//    ret = creat_connection();
//    if (ret != BLE_STATUS_SUCCESS) 
//    {
//        printf("creat_connection()--> Failed 0x%02x\r\n", ret);
//        while(1);
//    }
   
    while(1) 
    {
        /* Disable UART IRQ to avoid calling BLE stack functions while BTLE_StackTick() is running. */
        NVIC_DisableIRQ(UART_IRQn);

        /* BlueNRG-1 stack tick */
        BTLE_StackTick();

        NVIC_EnableIRQ(UART_IRQn);

        multi_connection();
        /* Application tick */
        user_task();

        /*  刷新 mti 的状态机 */
        mti_tick ();

    }
  
} /* end main() */

#ifdef  USE_FULL_ASSERT

/**
* @brief  Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param  file: pointer to the source file name
* @param  line: assert_param error line source number
*/
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
  ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  
  /* Infinite loop */
  while (1)
  {
  }
}

#endif

/******************* (C) COPYRIGHT 2015 STMicroelectronics *****END OF FILE****/
/** \endcond
 */
