/******************** (C) COPYRIGHT 2015 STMicroelectronics ********************
* File Name          : chat.c
* Author             : AMS - VMA RF  Application team
* Version            : V1.0.0
* Date               : 30-November-2015
* Description        : This file handles bytes received from USB and the init
*                      function. 
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "gp_timer.h"
#include "ble_const.h" 
#include "bluenrg1_stack.h"
#include "app_state.h"
#include "osal.h"
#include "gatt_uuid.h"
#include "connection_service.h"
#include "SDK_EVAL_Config.h"
#include "gp_timer.h"
#include "apps_program.h"
#include "ble_mutually.h"
/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/

#define CMD_BUFF_SIZE 512

/**************************全局变量********************************************/
uint8_t connInfo[20];

volatile int app_flags = SET_CONNECTABLE;


auth_Struct g_auth_state [MAX_NUM_BLE_CONNS];
typedef struct timer system_timer;
system_timer system_clock, auth_timer[MAX_NUM_BLE_CONNS]; //1S定时器用于两次广播
Bt_handle_t g_getble_handle;       //characteristic handle

index_handle g_index_handle;
uint8_t g_connet_num=0;
/**************************静态变量********************************************/
static const uint8_t authPassword[] = {0xff, 'e', 'c', 'o', 'm', 'o'};
static uint8_t s_advertisingData[23]=
{
    0x02,
    0x01,
    0x06,
    0x13,
    GAP_ADTYPE_LOCAL_NAME_COMPLETE,
    'C', 'C', ':',
    '7', '8', ':',
    'A', 'B', ':',
    '1', 'B', ':',
    '4', 'E', ':',
    '8', '0', 0,

}; 
static uint8_t s_scanRspData[] =
{
    ADVERTUUIDLEN,//17
    ADVERTUUIDTYPE,//7
    ECOMO_TAP_SERVICE_UUID_ARRAY,//b8510e22-7500-4608-bafe-6b72c2531e14
    // connection interval range
    0x05,   // length of this data
    GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE,
    LO_UINT16(DEFAULT_DESIRED_MIN_CONN_INTERVAL),   // 100ms
    HI_UINT16(DEFAULT_DESIRED_MIN_CONN_INTERVAL),
    LO_UINT16(DEFAULT_DESIRED_MAX_CONN_INTERVAL),   // 1s
    HI_UINT16(DEFAULT_DESIRED_MAX_CONN_INTERVAL),
    // Tx power level
    0x02,   // length of this data
    GAP_ADTYPE_POWER_LEVEL,
    0       // 0dBm
};
/**************************回调函数***************************************************/
static uint8_t EcomoTap_authenticatedCB (uint16_t connHandle);
static uint8_t EcomoTap_receiveDataCB (uint16_t connHandle, uint8_t len, uint8_t *pData);    
static EcomoTapCBs_t	*ecomoTapAppCBs = NULL;
// EcomoTap callbacks
static const EcomoTapCBs_t simpleBLEMulti_ecomoTapCBs =
{
	EcomoTap_authenticatedCB,
	EcomoTap_receiveDataCB
};
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void EcomoTap_addService( EcomoTapCBs_t *pAppCBs )
{
    uint8_t index ;
    for(index = 0; index < MAX_NUM_BLE_CONNS; index ++)
    {
        g_auth_state[index].is_Authenticated = FALSE;
    }
    ecomoTapAppCBs = pAppCBs;
}
 /***************************************************************************************
*	Function: void hex_to_str(char  *pbDest, const uint8_t *pbSrc, int nLen)
*	Description: 16进制转字符串
*	Input:  pbSrc - 输入16进制数的起始地址 nLen - 16进制数的字节数
*	Output: pbDest - 存放目标字符串
*	Return: 
*	Others: 2018.11.30
*****************************************************************************************/

void hex_to_str(char  *pbDest, const uint8_t *pbSrc, int nLen)
{
	char ddl,ddh;
	int i;

	for (i=0; i<nLen; i++)
	{
        ddh = 48 + pbSrc[i] / 16;
        ddl = 48 + pbSrc[i] % 16;
        if (ddh > 57) ddh = ddh + 7;
        if (ddl > 57) ddl = ddl + 7;
        pbDest[i*2] = ddh;
        pbDest[i*2+1] = ddl;
	}

	pbDest[nLen*2] = '\0';
}
 /***************************************************************************************
*	Function: copy_str(const char  *pbsrc)
*	Description: copy str 到s_advertisingData；
*	Input:  pbsrc - 输入字符串的起始地址
*	Output: 
*	Return: 
*	Others: 2018.11.30
*****************************************************************************************/
void copy_str(const char  *pbsrc)
{
    s_advertisingData[5]=pbsrc[10];
    s_advertisingData[6]=pbsrc[11];
    s_advertisingData[7]=':';
    s_advertisingData[8]=pbsrc[8];
    s_advertisingData[9]=pbsrc[9];
    s_advertisingData[10]=':';
    
    s_advertisingData[11]=pbsrc[6];
    s_advertisingData[12]=pbsrc[7];
    s_advertisingData[13]=':';
    s_advertisingData[14]=pbsrc[4];
    s_advertisingData[15]=pbsrc[5];
    s_advertisingData[16]=':';
    s_advertisingData[17]=pbsrc[2];
    s_advertisingData[18]=pbsrc[3];
    s_advertisingData[19]=':';
    s_advertisingData[20]=pbsrc[0];
    s_advertisingData[21]=pbsrc[1];
}
 /***************************************************************************************
*	Function: copy_str(const char  *pbsrc)
*	Description: copy str 到s_advertisingData；
*	Input:  pbsrc - 输入字符串的起始地址
*	Output: 
*	Return: 
*	Others: 2018.11.30
*****************************************************************************************/
void get_ble_mac(uint8_t *pdsrc)
{
    pdsrc[0] = *(uint8_t *)0x100007F4;
    pdsrc[1] = *(uint8_t *)0x100007F5;
    pdsrc[2] = *(uint8_t *)0x100007F6;
    pdsrc[3] = *(uint8_t *)0x100007F7;
    pdsrc[4] = *(uint8_t *)0x100007F8;
    pdsrc[5] = *(uint8_t *)0x100007F9; 
}
 /***************************************************************************************
*	Function: uint8_t fount_device_init(void)
*	Description: 蓝牙协议初始化
*	Input:  无
*	Output: 无
*	Return: 错误代码 0:SUCESS 
*	Others: 2018.11.30
*****************************************************************************************/
uint8_t fount_device_init(void)
{
    uint8_t ret;
    uint16_t service_handle;
    uint16_t dev_name_char_handle;
    uint16_t appearance_char_handle;
    uint8_t name[] = {'B', 'l', 'u', 'e', 'N', 'R', 'G', '1'};

    uint8_t role = GAP_PERIPHERAL_ROLE;
    uint8_t bdaddr[] = {0X80, 0X4E, 0X1B, 0XAB, 0X78, 0XCC};
    char dest[13]={0};
    get_ble_mac(bdaddr);
    hex_to_str(dest, bdaddr, 6);
    copy_str(dest);
    /* Configure Public address */
    ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, bdaddr);
    if(ret != BLE_STATUS_SUCCESS)
    {
        printf("Setting BD_ADDR failed: 0x%02x\r\n", ret);
        return ret;
    }

    /* Set the TX power to -2 dBm */
    aci_hal_set_tx_power_level(1, 7);

    /* GATT Init */
    ret = aci_gatt_init();
    if (ret != BLE_STATUS_SUCCESS)
    {
        printf ("Error in aci_gatt_init(): 0x%02x\r\n", ret);
        return ret;
    } 
    else
    {
        printf ("aci_gatt_init() --> SUCCESS\r\n");
    }

    /* GAP Init */
    ret = aci_gap_init(role, 0x00, 0x08, &service_handle, 
    &dev_name_char_handle, &appearance_char_handle);
    if (ret != BLE_STATUS_SUCCESS) 
    {
        printf ("Error in aci_gap_init() 0x%02x\r\n", ret);
        return ret;
    } 
    else 
    {
        printf ("aci_gap_init() --> SUCCESS\r\n");
    }

    /* Set the device name */
    ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle,
                                    0, sizeof(name), name);
    if (ret != BLE_STATUS_SUCCESS) 
    {
        printf ("Error in Gatt Update characteristic value 0x%02x\r\n", ret);
        return ret;
    } 
    else 
    {
        printf ("aci_gatt_update_char_value() --> SUCCESS\r\n");
    }

    ret = add_dev_info_att_service();
    if (ret != BLE_STATUS_SUCCESS) 
    {
        printf("Error in Add_Chat_Service 0x%02x\r\n", ret);
        return ret;
    } 
    else 
    {
        printf("Add_Chat_Service() --> SUCCESS\r\n");
    }
    EcomoTap_addService((EcomoTapCBs_t*)&simpleBLEMulti_ecomoTapCBs);
    return BLE_STATUS_SUCCESS;
}

/*******************************************************************************
*	Function: void creat_connection(void)
*	Description: 创建连接
*	Input: 无
*	Output: 无
*	Return: 错误代码 0:SUCESS 0XFF:err0
*	Others: 2018.11.30
*******************************************************************************/
uint8_t creat_connection(void)
{  
    tBleStatus ret;
    uint8_t status=0;
    uint8_t local_name[] = {AD_TYPE_COMPLETE_LOCAL_NAME,'e','c','o','m','0'};
    g_getble_handle = get_att_handle();
    ret = hci_le_set_scan_response_data(sizeof(s_scanRspData),s_scanRspData);
    if (ret != BLE_STATUS_SUCCESS)
    {
        status	=	0xff;
        printf ("Error in hci_le_set_scan_response_data(): 0x%02x\r\n", ret);
    }
    else
    {
        status	=	0;
        printf ("hci_le_set_scan_response_data() --> SUCCESS\r\n");
    }

    ret = aci_gap_set_discoverable(ADV_IND, 0x20, 0x100, PUBLIC_ADDR, NO_WHITE_LIST_USE,
                                    sizeof(local_name), local_name, 0, NULL, 0, 0);
    if (ret != BLE_STATUS_SUCCESS)
    {
        status	=	0xff;
        printf ("Error in aci_gap_set_discoverable(): 0x%02x\r\n", ret);
    }
    else
    {
        status	=	0;
        printf ("aci_gap_set_discoverable() --> SUCCESS\r\n");
    }
    ret = hci_le_set_advertising_data(sizeof(s_advertisingData),s_advertisingData);
    if (ret != BLE_STATUS_SUCCESS)
    {
        status	=	0xff;
        printf ("Error in hci_le_set_advertising_data(): 0x%02x\r\n", ret);
    }
    else
    {
        status	=	0;
        printf ("hci_le_set_advertising_data() --> SUCCESS\r\n");
    }
    
    return status;
}


/*******************************************************************************
*	Function: void index_distribution(uint16_t Connection_Handle)
*	Description: 根据连接句柄分配放入数组
*	Input: 无
*	Output: 无
*	Return: 错误代码 0:SUCESS 0XFF:err0
*	Others: 2018.11.30
*******************************************************************************/
void  index_distribution(uint16_t Connection_Handle)
{
    uint8_t index=0;
    for(index=0;index<MAX_NUM_BLE_CONNS;index++)
    {
        if(g_index_handle.connet_flag[index] == 0)
        {
            g_index_handle.connet_handle[index] = Connection_Handle;
            g_index_handle.connet_flag[index] = 1;
             Timer_Set(&auth_timer[index], 10000); //10S后判断是否认证通过
            break;
        }
    }
}
/*******************************************************************************
*	Function: void  index_delete(uint16_t Connection_Handle)
*	Description: 删除数组的句柄
*	Input: 无
*	Output: 无
*	Return: 错误代码 0:SUCESS 0XFF:err0
*	Others: 2018.11.30
*******************************************************************************/
void  index_delete(uint16_t Connection_Handle)
{
    uint8_t index=0;
    for(index=0;index<MAX_NUM_BLE_CONNS;index++)
    {
        if(g_index_handle.connet_handle[index] == Connection_Handle)
        {
            g_index_handle.connet_handle[index] = 0;
            g_index_handle.connet_flag[index] = 0;
            break;
        }
    }

}
/*******************************************************************************
*	Function: uint8_t  get_index(uint16_t Connection_Handle)
*	Description: 得到句柄的索引
*	Input: 无
*	Output: 无
*	Return: 0xff错误  
*	Others: 2018.11.30
*******************************************************************************/
uint8_t  get_index(uint16_t Connection_Handle)
{
    uint8_t index=0;
    for(index=0;index<MAX_NUM_BLE_CONNS;index++)
    {
        if(g_index_handle.connet_handle[index] == Connection_Handle)
        {
            return index;
        }
    }
    
    return 0xff;
} 
/*******************************************************************************
*	Function: uint8_t EcomoTap_authenticatedCB (uint16_t connHandle)
*	Description: 队列放入句柄
*	Input: 句柄
*	Output: 无
*	Return: 错误代码 0:SUCESS  1:err
*	Others: 2018.11.30
**************************************************/
static uint8_t EcomoTap_authenticatedCB (uint16_t connHandle)
{
	//return	MTI_connect (connHandle);
}
/*******************************************************************************
*	Function: uint8_t EcomoTap_receiveDataCB (uint16_t connHandle, uint8_t len, uint8_t *pData)
*	Description: 队列放入句柄的数据
*	Input: connHandle ：连接句柄 len ：长度 pData：数据
*	Output: 无
*	Return: 错误代码 0:SUCESS  1:err
*	Others: 2018.11.30
**************************************************/
static uint8_t EcomoTap_receiveDataCB (uint16_t connHandle, uint8_t len, uint8_t *pData)
{
	return MTI_sendData (MTI_CHANNEL_REQUEST, connHandle, len, pData);
    
}
/* ***************** BlueNRG-1 Stack Callbacks ********************************/

/*******************************************************************************
* Function Name  : hci_le_connection_complete_event.
* Description    : This event indicates that a new connection has been created.
* Input          : See file bluenrg1_events.h
* Output         : See file bluenrg1_events.h
* Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)

{ 
    index_distribution(Connection_Handle);
    g_connet_num++;
    set_connet_num_flag(g_connet_num);
    Timer_Set(&system_clock, 1000);
    connet_event_flag_set(CONNECTED);

    
}/* end hci_le_connection_complete_event() */

/*******************************************************************************
* Function Name  : hci_disconnection_complete_event.
* Description    : This event occurs when a connection is terminated.
* Input          : See file bluenrg1_events.h
* Output         : See file bluenrg1_events.h
* Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_disconnection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Reason)
{ 

    g_connet_num--;
    index_delete(Connection_Handle);
    set_connet_num_flag(g_connet_num);
    if(g_connet_num == 1)
    {   
        Timer_Set(&system_clock, 1000);
        connet_event_flag_set(CONNECTED);
    }

    /* Make the device connectable again. */
    APP_FLAG_SET(SET_CONNECTABLE);
    APP_FLAG_CLEAR(NOTIFICATIONS_ENABLED);
    APP_FLAG_CLEAR(TX_BUFFER_FULL);

    APP_FLAG_CLEAR(START_READ_TX_CHAR_HANDLE);
    APP_FLAG_CLEAR(END_READ_TX_CHAR_HANDLE);
    APP_FLAG_CLEAR(START_READ_RX_CHAR_HANDLE); 
    APP_FLAG_CLEAR(END_READ_RX_CHAR_HANDLE);
  
}/* end hci_disconnection_complete_event() */


/*******************************************************************************
 * Function Name  : aci_gatt_attribute_modified_event.
 * Description    : This event occurs when an attribute is modified.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint16_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{   
	 uint8_t i=0;
    if(Attr_Handle ==	g_getble_handle.apprxhandle +  1)
    {
        if(g_auth_state[get_index(Connection_Handle)].is_Authenticated)
        {
            // already authenticated
            if (ecomoTapAppCBs != NULL && ecomoTapAppCBs->pfnReceiveData != NULL)
            {
                uint8_t status = (ecomoTapAppCBs->pfnReceiveData)(Connection_Handle, Attr_Data_Length, Attr_Data);
                if (status != SUCCESS)
                {
                    aci_gatt_write_resp(Connection_Handle,Attr_Handle,0x01,0x0e,Attr_Data_Length,Attr_Data);

                }
                else
                {
                    /*********切换到com_data_pross_event处理数据***/ 
                    system_event_set(app_data_proc_event);
                }    
            }
            else 
            {

               aci_gatt_write_resp(Connection_Handle,Attr_Handle,0x01,0x0e,Attr_Data_Length,Attr_Data);
            }
        }
        else
        {
            
             {
                 // try to authenticate
                  if (Attr_Data_Length == sizeof(authPassword)
                    && memcmp(Attr_Data, authPassword, sizeof(authPassword)) == 0)
                  {
                      uint8_t status = SUCCESS;
                      if (ecomoTapAppCBs != NULL && ecomoTapAppCBs->pfnAuthenticated != NULL)
                      {
                          status = (ecomoTapAppCBs->pfnAuthenticated) (Connection_Handle);
                          /*********切换到com_data_pross_event处理数据***/
                          system_event_set(app_data_proc_event);
                      }
                      if (status == SUCCESS)
                      {

                          g_auth_state[get_index(Connection_Handle)].is_Authenticated = TRUE;
                      } 
                      else 
                      {
                         aci_gatt_write_resp(Connection_Handle,Attr_Handle,0x01,0x0e,Attr_Data_Length,Attr_Data);
                      }

                  }
                 for(i=0;i<Attr_Data_Length;i++)
                 {
                     printf("%d\n",Attr_Data[i]);
                 }
             }
        }
    }

	if(Attr_Handle ==	(g_getble_handle.appindicatehandle +  2) )
	 {
		 for(i=0;i<Attr_Data_Length;i++)
		 {
			 printf("%d\n",Attr_Data[i]);
		 }
		 
	 }		 
}


/*******************************************************************************
 * Function Name  : aci_gatt_tx_pool_available_event.
 * Description    : This event occurs when a TX pool available is received.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_tx_pool_available_event(uint16_t Connection_Handle,
                                      uint16_t Available_Buffers)
{       
  /* It allows to notify when at least 2 GATT TX buffers are available */
  APP_FLAG_CLEAR(TX_BUFFER_FULL);
} 

/*******************************************************************************
 * Function Name  : aci_gatt_read_permit_req_event.
 * Description    : This event is given when a read request is received
 *                  by the server from the client.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_read_permit_req_event(uint16_t Connection_Handle,
                                    uint16_t Attribute_Handle,
                                    uint16_t Offset)
{  
    if( g_auth_state[get_index(Connection_Handle)].is_Authenticated == DISABLE)
    {
       aci_gatt_deny_read(Connection_Handle,0X08);//禁止读 
    }
    else
    {
       aci_gatt_allow_read(Connection_Handle);//允许读
    }
}

void aci_hal_end_of_radio_activity_event(uint8_t Last_State,
                                         uint8_t Next_State,
                                         uint32_t Next_State_SysTime)
{

}
