
#include <stdio.h>
#include <string.h>
#include "ble_const.h" 
#include "bluenrg1_stack.h"
#include "osal.h"
#include "app_state.h"
#include "SDK_EVAL_Config.h"
#include "connection_service.h"
#include "gatt_uuid.h"
#include "ble_mutually.h"
#define COPY_UUID_128(uuid_struct, uuid_15, uuid_14, uuid_13, uuid_12, uuid_11, uuid_10, uuid_9, uuid_8, uuid_7, uuid_6, uuid_5, uuid_4, uuid_3, uuid_2, uuid_1, uuid_0) \
do {\
  uuid_struct.uuid128[0] = uuid_0; uuid_struct.uuid128[1] = uuid_1; uuid_struct.uuid128[2] = uuid_2; uuid_struct.uuid128[3] = uuid_3; \
	uuid_struct.uuid128[4] = uuid_4; uuid_struct.uuid128[5] = uuid_5; uuid_struct.uuid128[6] = uuid_6; uuid_struct.uuid128[7] = uuid_7; \
	uuid_struct.uuid128[8] = uuid_8; uuid_struct.uuid128[9] = uuid_9; uuid_struct.uuid128[10] = uuid_10; uuid_struct.uuid128[11] = uuid_11; \
	uuid_struct.uuid128[12] = uuid_12; uuid_struct.uuid128[13] = uuid_13; uuid_struct.uuid128[14] = uuid_14; uuid_struct.uuid128[15] = uuid_15; \
	}while(0)
/**************************全局变量************************************/
Bt_handle_t g_chatHandle;     //characteristic handle
Service_UUID_t service_uuid; //service UUID
Char_UUID_t char_uuid;       //characteristic UUID

 /************************************************************************************************************************************************
*	Function: addChatService(void)
*	Description: 添加服务
*	Input: 无
*	Output: 无
*	Return: 错误代码
*	Others: 2018.11.30
*************************************************************************************************************************************************/
uint8_t add_dev_info_att_service(void)
{
    uint8_t ret = 0;
    // EcomoTap GATT Profile Service UUID: "b8510e22-7500-4608-bafe-6b72c2531e14"
    uint8_t app_serice_uuid[16]={0x14, 0x1e, 0x53, 0xc2, 0x72, 0x6b, 0xfe, 0xba, 0x08, 0x46, 0x00, 0x75, 0x22, 0x0e, 0x51, 0xb8};

    // EcomoTap GATT Profile Characteristic CONTROL UUID: "97af7c9d-a2a5-40a0-a915-68cad5ba086e"
    uint8_t app_char_tx_uuid[16]={0x6e, 0x08, 0xba, 0xd5, 0xca, 0x68, 0x15, 0xa9, 0xa0, 0x40, 0xa5, 0xa2, 0x9d, 0x7c, 0xaf, 0x97};

    // EcomoTap GATT Profile Characteristic RESPONSE UUID: "05f7c53c-67dc-4d35-a4f4-c80caa8e39bd"
    uint8_t app_char_response_uuid[16]={0xbd, 0x39, 0x8e, 0xaa, 0x0c, 0xc8, 0xf4, 0xa4, 0x35, 0x4d, 0xdc, 0x67, 0x3c, 0xc5, 0xf7, 0x05};

    // EcomoTap GATT Profile Characteristic VERSION UUID: "400b6a46-51bb-434a-95b1-0d5c9caa7380"
    uint8_t app_char_rx_uuid[16]={0x80, 0x73, 0xaa, 0x9c, 0x5c, 0x0d, 0xb1, 0x95, 0x4a, 0x43, 0xbb, 0x51, 0x46, 0x6a, 0x0b, 0x40};

    service_uuid.Service_UUID_16=0X180a;
    ret = aci_gatt_add_service(UUID_TYPE_16, &service_uuid, PRIMARY_SERVICE, 20, &(g_chatHandle.devinfoservehandle) ); 
    if (ret != BLE_STATUS_SUCCESS) goto fail;    

    char_uuid.Char_UUID_16=0X2A23;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.idSystemHandle) );
    if (ret != BLE_STATUS_SUCCESS) goto fail;

    char_uuid.Char_UUID_16=0X2A24;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.modelNumbHandle) );
                                
    char_uuid.Char_UUID_16=0X2A25;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.serialNumberHandle) );
                                
    char_uuid.Char_UUID_16=0X2A26;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.firmwareRevHandle) );
                                
    char_uuid.Char_UUID_16=0X2A27;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.hardwareRevHandle) );	
    char_uuid.Char_UUID_16=0X2A28;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.softWareRevHandle) );		
    char_uuid.Char_UUID_16=0X2A29;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.devInfoMfrNamePropsHandle) );
    char_uuid.Char_UUID_16=0X2A2A;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.ieee11073Data) );									
    char_uuid.Char_UUID_16=0X2A50;
    ret =  aci_gatt_add_char(g_chatHandle.devinfoservehandle, UUID_TYPE_16, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.pnpIDHandle) );	
    if (ret != BLE_STATUS_SUCCESS) goto fail;

    /*ecomo 定义用户服务**************/
    Osal_MemCpy(&service_uuid.Service_UUID_128, app_serice_uuid, 16);
    ret = aci_gatt_add_service(UUID_TYPE_128, &service_uuid, PRIMARY_SERVICE, 8, &(g_chatHandle.appservehandle)); 
    if (ret != BLE_STATUS_SUCCESS) goto fail; 

    Osal_MemCpy(&char_uuid.Char_UUID_128, app_char_tx_uuid, 16);
    ret =  aci_gatt_add_char(g_chatHandle.appservehandle, UUID_TYPE_128, &char_uuid, 20, CHAR_PROP_WRITE, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                16, 1, &(g_chatHandle.apprxhandle));
                                
    Osal_MemCpy(&char_uuid.Char_UUID_128, app_char_response_uuid, 16);
    ret =  aci_gatt_add_char(g_chatHandle.appservehandle, UUID_TYPE_128, &char_uuid, 20, CHAR_PROP_INDICATE, ATTR_PERMISSION_NONE, 0,
                16, 1, &(g_chatHandle.appindicatehandle));							
    if (ret != BLE_STATUS_SUCCESS) goto fail;

    Osal_MemCpy(&char_uuid.Char_UUID_128, app_char_rx_uuid, 16);
    ret =  aci_gatt_add_char(g_chatHandle.appservehandle, UUID_TYPE_128, &char_uuid, 20, CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                16, 1, &(g_chatHandle.apptxhandle));								
    if (ret != BLE_STATUS_SUCCESS) goto fail;
    
 
    return BLE_STATUS_SUCCESS;
fail:
    printf("Error while adding Chat service.\n");
    return BLE_STATUS_ERROR ;
}

 /******************************************************************************************************************
*	Function: uint8_t  gatt_update_notify_value (uint8_t len, uint8_t *pData)
*	Description: ecomo_set_version
*	Input: len:要写的长度，value:要写如的值
*	Output: 无
*	Return:  0 成功 1 失败 BLE_STATUS_INSUFFICIENT_RESOURCES:协议栈堆栈溢出
*	Others: 2018.11.30
*******************************************************************************************************************/
uint8_t  gatt_update_notify_value (uint8_t channel, uint16_t connHandle, uint8_t len, uint8_t *pData)
{
    uint8_t status;
    switch (channel)
    {
    case	MTI_CHANNEL_RESPONSE:
        status = aci_gatt_update_char_value_ext (connHandle,g_chatHandle.appservehandle,g_chatHandle.appindicatehandle,
                                                 0x02,20,0,len,pData);
        break;
    default:
        break;
    }
    return  status;
}
 /******************************************************************************************************************
*	Function: uint8_t  ecomo_set_version (uint8_t len, uint8_t *pData)
*	Description: ecomo_set_version
*	Input: len:要写的长度，value:要写如的值
*	Output: 无
*	Return:  0 成功 1 失败 BLE_STATUS_INSUFFICIENT_RESOURCES:协议栈堆栈溢出
*	Others: 2018.11.30
*******************************************************************************************************************/
uint8_t  ecomo_set_version (uint8_t len, uint8_t *pData)
{
    uint8_t status;
	if (len < ECOMO_VERSION_MAX_BYTES)
	{

        status = aci_gatt_update_char_value(g_chatHandle.appservehandle,g_chatHandle.apptxhandle,0,len,pData);
 	} 
    else 
    {
 		status =0xff;
 	}
    return status;
}
 /******************************************************************************************************************
*	Function: uint8_t set_dev_info_parmeter(uint8_t Char_Handle,uint8_t len,uint8_t *value)
*	Description: 向Deveice Infomation 服务的特征值写如参数
*	Input: Char_Handle:特征值对应的句柄，len:要写的长度，value:要写如的值
*	Output: 无
*	Return:  0 成功 1 失败 BLE_STATUS_INSUFFICIENT_RESOURCES:协议栈堆栈溢出
*	Others: 2018.11.30
*******************************************************************************************************************/
uint8_t set_dev_info_parmeter(uint8_t decinfoname,uint8_t len,uint8_t *value)
{
	uint8_t ret;
	if(len > DEVINFO_STR_ATTR_LEN)
	{
		return BLEINVALIDRANGE;
	}
	switch(decinfoname)
	{
		case DEVINFO_SYSTEM_ID:
			if(len > 8)
			{
				return BLEINVALIDRANGE;
			}
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.idSystemHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_MODEL_NUMBER:

			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.modelNumbHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_FIRMWARE_REV:
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.firmwareRevHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_SERIAL_NUMBER:
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.serialNumberHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_HARDWARE_REV:
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.hardwareRevHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_SOFTWARE_REV:
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.softWareRevHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_MANUFACTURER_NAME:
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.devInfoMfrNamePropsHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_11073_CERT_DATA:
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.ieee11073Data,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;
		case DEVINFO_PNP_ID:
			ret = aci_gatt_update_char_value(g_chatHandle.devinfoservehandle,g_chatHandle.pnpIDHandle,0,len,value);
			if(ret  != STATUS_SUCCESS)
			{
				ret = 0;
			}
			break;	
		default:
			return 1;	
	}
	return ret;
}
 /*************************************************
*	Function: Bt_handle_t getChatHandle(void)
*	Description: 得到特征值句柄
*	Input: 无
*	Output: 无
*	Return: Bt_handle_t 类型结构句柄
*	Others: 2018.11.30
**************************************************/
Bt_handle_t get_att_handle(void)
{
	return g_chatHandle;
}

