#include "fount_spi.h"
#include "BlueNRG1_conf.h"
#include "SDK_EVAL_Config.h"
#include "ble_mutually.h"
#include "BlueNRG_x_device.h"
#include <string.h>

static void spi_gpio_config(void);
static void spi_config(void);
static void spi_dma_config(void);
static void spi_nvic_config(void);
static void dma_transfer_cmd(uint8_t status);
static void set_dma_size(uint16_t size);
/**************************全局变量************************************/
volatile uint8_t spi_buffer_tx[20];
uint8_t spi_buffer_rx[20];
uint8_t spi_dma_size;
static volatile uint8_t spi_busy = SPI_TRANSFER_IDLE;
uint8_t rec_len=0;
  /********************************************************************
*	Function: void spi_configuration(void)
*	Description: spi配置
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
***********************************************************************/
void spi_init(void)
{
    spi_gpio_config();
    
    spi_config();
    
    spi_dma_config();
    
    spi_nvic_config();
}
  /********************************************************************
*	Function: void spi_configuration(void)
*	Description: spi配置
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
***********************************************************************/
static void spi_gpio_config(void)
{

    GPIO_InitType GPIO_InitStructure;

    /* Enable SPI and GPIO clocks */
    SysCtrl_PeripheralClockCmd(CLOCK_PERIPH_GPIO | CLOCK_PERIPH_SPI, ENABLE);   

    /* Configure SPI pins */
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = SPI_OUT_PIN;  //DIO2
    GPIO_InitStructure.GPIO_Mode = SPI_OUT_MODE;
    GPIO_InitStructure.GPIO_Pull = ENABLE;
    GPIO_InitStructure.GPIO_HighPwr = DISABLE;
    GPIO_Init(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = SPI_IN_PIN; //DIO3
    GPIO_InitStructure.GPIO_Mode = SPI_IN_MODE;
    GPIO_Init(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = SPI_CLK_PIN;//DIO8
    GPIO_InitStructure.GPIO_Mode = SPI_CLK_MODE;
    GPIO_Init(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = SPI_CS_PIN; //DIO 11
    GPIO_InitStructure.GPIO_Mode = GPIO_Output;
    GPIO_InitStructure.GPIO_HighPwr = ENABLE;
    GPIO_Init(&GPIO_InitStructure);
    GPIO_SetBits(SPI_CS_PIN);


}
  /********************************************************************
*	Function: void void spi_config(void)
*	Description: spi  配置
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
***********************************************************************/
static void spi_config(void)
{
    SPI_InitType SPI_InitStructure;
        /* Configure SPI in master mode */
    SPI_StructInit(&SPI_InitStructure);
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b ;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_BaudRate = SPI_BAUDRATE;
    SPI_Init(&SPI_InitStructure);

    /* Clear RX and TX FIFO */
    SPI_ClearTXFIFO();
    SPI_ClearRXFIFO();

    /* Set null character */
    SPI_SetDummyCharacter(0xFF);
        /* Set communication mode */
    SPI_SetMasterCommunicationMode(SPI_FULL_DUPLEX_MODE);
}
  /********************************************************************
*	Function: void void spi_dma_config(void)
*	Description: spi dma 配置
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
***********************************************************************/
static void spi_dma_config(void)
{
    DMA_InitType DMA_InitStructure;
    
    /* Configure DMA peripheral */
    SysCtrl_PeripheralClockCmd(CLOCK_PERIPH_DMA, ENABLE);
    /* Configure DMA SPI TX channel */
    DMA_InitStructure.DMA_PeripheralBaseAddr = SPI_DR_BASE_ADDR;    //指定DMAy Channelx的外围基址
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)spi_buffer_tx;  
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;              //数据传输方向定义
    DMA_InitStructure.DMA_BufferSize = (uint32_t)SPI_BUFF_SIZE;     //大小
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //是否增加外围地址寄存器。
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;          //否增加内存地址寄存器。
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;//外围数据宽度。
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;  //内存数据宽度。
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  
    DMA_Init(DMA_CH_SPI_TX, &DMA_InitStructure);

    /* Configure DMA SPI RX channel */
    DMA_InitStructure.DMA_PeripheralBaseAddr = SPI_DR_BASE_ADDR;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)spi_buffer_rx;  
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = (uint32_t)SPI_BUFF_SIZE;  
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;  
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;   
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;

    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;

    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;  
    DMA_Init(DMA_CH_SPI_RX, &DMA_InitStructure);   


    /* Enable SPI_TX/SPI_RX DMA requests */
    SPI_DMACmd(SPI_DMAReq_Tx | SPI_DMAReq_Rx, ENABLE);

    /* Enable DMA_CH_UART_TX Transfer Complete interrupt */
    DMA_FlagConfig(DMA_CH_SPI_TX, DMA_FLAG_TC, ENABLE);
    
    DMA_FlagConfig(DMA_CH_SPI_RX,DMA_FLAG_TC,ENABLE);
    

    /* Configure DMA SPI RX channel */
    DMA_Cmd(DMA_CH_SPI_RX, DISABLE);
    
    DMA_Cmd(DMA_CH_SPI_TX, DISABLE);
    /* Enable SPI functionality */
    SPI_Cmd(DISABLE);   
  
}
  /********************************************************************
*	Function: void void spi_nvic(void)
*	Description: spi nvic 配置
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
***********************************************************************/
static void spi_nvic_config(void)
{
    NVIC_InitType NVIC_InitStructure;
    /* Enable the USARTx Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = DMA_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = HIGH_PRIORITY;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);  
}
  /********************************************************************
*	Function: dma_transfer_cmd(uint8_t status)
*	Description: DMA发送使能
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
***********************************************************************/
static void dma_transfer_cmd(uint8_t status)
{
    switch(status)
    {
        case ENABLE:
                DMA_Cmd(DMA_CH_SPI_RX, ENABLE);
    
                DMA_Cmd(DMA_CH_SPI_TX, ENABLE);
                /* Enable SPI functionality */
                SPI_Cmd(ENABLE); 
        break;
        case DISABLE:
                DMA_Cmd(DMA_CH_SPI_RX, DISABLE);
    
                DMA_Cmd(DMA_CH_SPI_TX, DISABLE);
                /* Enable SPI functionality */
                SPI_Cmd(DISABLE); 
        break;
    }
}
  /********************************************************************
*	Function: set_dma_size(uint16_t size)
*	Description: 设置DMA要发送的字节
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
***********************************************************************/
static void set_dma_size(uint16_t size)
{
    DMA_SetCurrDataCounter(DMA_CH_SPI_TX, size);
    DMA_SetCurrDataCounter(DMA_CH_SPI_RX, size);
}
/**********************************************************************
*	Function: uint8_t spi_read_data( uint8_t* pbuffer, uint8_t len)
*	Description: SPI读数据
*	Input: pbuffer：要读的数组 len 要读的数据长度
*	Output: 无
*	Return: 
*	Others: 2018.11.30
***********************************************************************/
uint8_t spi_read_data( uint8_t* pBuffer, uint8_t NumByteToRead )
{
  memcpy(pBuffer,spi_buffer_rx,NumByteToRead);
  return NumByteToRead;
}
  /********************************************************************
*	Function: transfer_status_t spi_send( uint8_t* pbuffer, uint8_t len)
*	Description: SPI数据发送
*	Input: pbuffer：要发送的数组 len 要发送的长度
*	Output: 无
*	Return: transfer_busy 忙 overflow 长度越界 transfer_success 失败
*	Others: 2018.11.30
***********************************************************************/
spi_status_t spi_send( uint8_t* pbuffer, uint8_t len)
{
    /******************** 设置为全双工*****************/
    SPI_SetMasterCommunicationMode(SPI_FULL_DUPLEX_MODE);
    if(len > MTI_PDU_MAX_DATA)
    {
        return SPI_INVALID_LENGTH;
    }
    rec_len =len;
    if(spi_busy == SPI_TRANSFER_BUSY)
    {
        return SPI_SEND_BUSY;
    }
    /*********赋值到要传输的数组**********************/
    for(uint8_t i=0;i<len;i++) 
    {
        spi_buffer_tx[i] = pbuffer[i];
    }
    spi_busy = SPI_TRANSFER_BUSY;

    /* 设置要发送的数据大小*/
    set_dma_size(len);
    /* 接收发送使能*/
    dma_transfer_cmd(ENABLE);
    return SPI_SEND_SUCCESS;
}
/**********************************************************************
*	Function:uint8_t is_spi_busy(void)
*	Description: 
*	Input: 
*	Output: 无
*	Return: 返回spi状态是否忙
*	Others: 2018.11.30
***********************************************************************/
uint8_t is_spi_busy(void)
{
    return spi_busy;
}

/**********************************************************************
*	Function:void busy_reset(void)
*	Description: 
*	Input: 
*	Output: 无
*	Return: 返回设置状态
*	Others: 2018.11.30
***********************************************************************/
uint8_t  busy_reset(void)
{
    spi_busy = SPI_TRANSFER_IDLE;
    return spi_busy;
}
  /********************************************************************
*	Function:uint8_t get_spi_busy_status
*	Description: 得到spi是否忙
*	Input: 
*	Output: 无
*	Return: transfer_busy 忙 transfer_idly 空闲
*	Others: 2018.11.30
***********************************************************************/
uint8_t   get_spi_busy_status(void)
{
     if(spi_busy == SPI_TRANSFER_BUSY)
         return SPI_TRANSFER_BUSY;
     else
         return SPI_TRANSFER_IDLE;
}
/**********************************************************************
*	Function:void spi_cs_control(uint8_t status)
*	Description: spi_cs控制
*	Input: 
*	Output: 无
*	Return: 
*	Others: 2018.11.30
***********************************************************************/
void spi_cs_control(uint8_t status)
{
    switch(status)
    {
        case ENABLE:
            GPIO_ResetBits(SPI_CS_PIN);
            break;
        case DISABLE:
            GPIO_SetBits(SPI_CS_PIN);
            break;
    }
}
/**********************************************************************
*	Function:DMA_Handler(void)
*	Description: DMA中断
*	Input: 
*	Output: 无
*	Return: 
*	Others: 2018.11.30
***********************************************************************/
void DMA_Handler(void)
{  
    if(DMA_GetFlagStatus(DMA_CH_SPI_RX_IT_TC)) 
    {
        /* Clear pending bit */
        DMA_ClearFlag(DMA_CH_SPI_RX_IT_TC);
        /* DMA_SPI_RX disable */
        DMA_Cmd(DMA_CH_SPI_RX, DISABLE);
        /* 设置为空闲标志 */
        spi_busy = SPI_TRANSFER_IDLE;
    }
    /* Check DMA_CH_UART_TX Transfer Complete interrupt */
    if(DMA_GetFlagStatus(DMA_CH_SPI_TX_IT_TC))
    {
        DMA_ClearFlag(DMA_CH_SPI_TX_IT_TC);

        DMA_Cmd(DMA_CH_SPI_TX, DISABLE);
    }

}
