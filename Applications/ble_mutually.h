/*************************************************
Copyright (C), 2018-2018, Ecomo Tech. Co., Ltd.
File name:  ble_mutually.h
Author: 靳杨超        Version: V1.0       Date: 2018-12-17
Description:    负责维护与MSP430通信的发送缓冲区，以及接收到的数据的处理
Others:
Function List:  // 主要函数列表，每条记录应包括函数名及功能简要说明


History:
    1.  Version: V1.0
        Date: 2018-12-17
        Author: 靳杨超
        Modification: 初次编码
    2.  Version: V1.1
        Date:2018-12-17
        Author: 王明浩
        Modification: 使用状态机重构
*************************************************/



#ifndef _BLE_MUTUALLY_H_
#define _BLE_MUTUALLY_H_


#include <stdint.h>




//#define SUCCESS                   0x00
#define FAILURE                     0x01
#define	MTI_BLE_MAX_DATA			20

//	主通道代码
#define	MTI_CHANNEL_REQUEST			0
#define	MTI_CHANNEL_RESPONSE		1
#define	MTI_CHANNEL_SYSTEM			0xff

//	系统服务代码
#define	MTI_SYSTEM_SEND_ADDRESS		0x00
#define	MTI_SYSTEM_VERSION			0x01
#define	MTI_SYSTEM_REBOOT			0x02
#define	MTI_SYSTEM_CONNECT			0x03
#define	MTI_SYSTEM_DISCONNECT		0x04
#define	MTI_SYSTEM_CONFIRM			0x05
#define	MTI_SYSTEM_MAX_CONNS		0x06
#define	MTI_SYSTEM_DEVINFO			0x07

#define	MTI_PDU_MAX_DATA		(MTI_BLE_MAX_DATA + 3)
#define	MTI_PACKAGE_MAX_DATA	(MTI_PDU_MAX_DATA + 2)

#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)
#define BUILD_UINT16(loByte, hiByte) \
          ((uint16_t)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define	MTI_TRANSFER_MAX_RETRY	255

/**
 * @brief  SPI buffer max size
 */


// callback when a connection is authenticated
typedef uint8_t (*EcomoTap_authenticated_t) (uint16_t connHandle);

// callback when data is received after a connection was authenticated
typedef uint8_t (*EcomoTap_receiveData_t) (uint16_t connHandle, uint8_t len, uint8_t *pValue);

// Callback when mcu response for request version
typedef uint8_t (*MTI_responseVersion) (uint8_t length, uint8_t *pVersion);

// Callback when mcu response for max connection request
typedef uint8_t (*MTI_responseMaxConns) (uint8_t maxConns);
// Callback when mcu set device information
typedef uint8_t (*MTI_setDevInfo) (uint8_t id, uint8_t length, uint8_t *pData);

// Callback when receive data
typedef uint8_t (*MTI_receiveData) (uint8_t channel, uint16_t connHandle, uint8_t length, uint8_t *pData);

typedef struct {
    EcomoTap_authenticated_t	pfnAuthenticated;
    EcomoTap_receiveData_t		pfnReceiveData;
} EcomoTapCBs_t;

typedef struct
{
	MTI_responseVersion		pfn_response_version;	// called when response version
	MTI_responseMaxConns	pfn_response_max_conns;
	MTI_setDevInfo			pfn_set_devInfo;
	MTI_receiveData			pfn_receiv_edata; 	// Called when receive data
} MTICBs_t;

void queue_init(void);
uint8_t MTI_connect (uint16_t connHandle);
uint8_t MTI_sendData (uint8_t channel, uint16_t connHandle, uint8_t length, uint8_t *pData);
uint8_t mti_queue_put(uint16_t len,uint8_t *pdata);
uint8_t mti_queue_rev(uint16_t *size, uint8_t  *buffer);
uint8_t mti_transfer (uint8_t length, uint8_t *pSend);
uint8_t mti_process_rcv_data (uint8_t length, uint8_t *pData);

uint8_t mti_request_max_conns (void);
uint8_t mti_process_rcv_data (uint8_t length, uint8_t *pData);

#endif






