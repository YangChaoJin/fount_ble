/*************************************************
Copyright (C), 2018-2018, Ecomo Tech. Co., Ltd.
File name:  transfer.c
Author: 王明浩        Version: V1.0       Date: 2018-12-17
Description:    负责与MSP430的数据交换，包括数据传输和重试
Others:
Function List:  // 主要函数列表，每条记录应包括函数名及功能简要说明
    1. transfer_init 初始化传输管理
    2. transfer_tick 状态机切换入口
    3. transfer_send 申请发送数据
    4. transfer_is_busy 检查传输是否完毕
    5. transfer_read 读取传输结果

History:
    1.  Version: V1.0
        Date: 2018-12-17
        Author: 靳杨超
        Modification: 初次编码
*************************************************/


#include "transfer.h"
#include "fount_spi.h"
#include "ble_mutually.h"
#include <string.h>
#include "gp_timer.h"
#define  WAIT_TIME      1000
#define  TRANSFER_BUSY_STATUS   0
#define  TRANSFER_IDLY_STATUS   1
typedef struct
{
    uint8_t send_data[TRANSFER_SEND_SIZE];
    uint8_t send_len;
    uint8_t read_data[TRANSFER_SEND_SIZE];
    uint8_t read_len;
    uint8_t get_data[TRANSFER_SEND_SIZE];
    uint8_t get_len;
    uint8_t remainLength;
    uint8_t retry;
}transfer_data_t;
typedef enum
{
    EMPTY_STATUS,
    START_STATUS,
    FIRST_SEND_STATUS,
    SECOND_SEND_STATUS,
    RECEIVE_STATUS,
    END_STATUS,
    HOLD_STATUS,
    FAIL_STATUS,
    SUCESS_STATUS,
}transfer_status_t;

uint8_t g_transfer_busy ; //传输空闲
struct timer status_timer; 
transfer_data_t g_transfer; //传输数据结构
transfer_status_t e_transfer_status; //状态
static uint8_t	checksum (uint8_t *pdat, uint8_t size);
static void set_transfer_status(bool status);
/*************************************************
 * Function:    transfer_init
 * Description: 初始化传输协议层
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    transfer_init (void)
{
    spi_init();
    e_transfer_status = EMPTY_STATUS;//设为空闲状态
    set_transfer_status(TRANSFER_IDLY_STATUS);//设置传输空闲 
}

/*************************************************
 * Function:    transfer_tick
 * Description: 状态机切换工作入口
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    transfer_tick (void)
{
    spi_status_t spi_status;
    uint8_t rcvLength =0;
    switch(e_transfer_status)
    {
        case EMPTY_STATUS:
            
        break;
        
        case SUCESS_STATUS:
            
            if(Timer_Expired(&status_timer))
            {
                e_transfer_status = EMPTY_STATUS; 
                set_transfer_status(TRANSFER_IDLY_STATUS);//设置传输空闲
            }
            
        break;
            
        case HOLD_STATUS:
            
            if(Timer_Expired(&status_timer))
            {
               e_transfer_status = START_STATUS; 
               set_transfer_status(TRANSFER_IDLY_STATUS);//设置传输空闲 
              
            }
            
        break;
            
        case START_STATUS:
            
            spi_cs_control(ENABLE); //拉低CS管脚
            Timer_Set(&status_timer,1000);
            e_transfer_status = FIRST_SEND_STATUS;
        
        break;
        
        case FIRST_SEND_STATUS:
            
            if(Timer_Expired(&status_timer))
            {   
                set_transfer_status(TRANSFER_BUSY_STATUS);//设置传输忙
                spi_status = spi_send(g_transfer.send_data,1);
                if(spi_status == SPI_INVALID_LENGTH)
                {
                    break;
                }
                if(spi_status == SPI_SEND_BUSY) //等待发送
                {
                    e_transfer_status = FIRST_SEND_STATUS;
                }
                if(spi_status == SPI_SEND_SUCCESS)
                {
                    e_transfer_status = SECOND_SEND_STATUS;
                }
            }
            
        break;
            
        case SECOND_SEND_STATUS:
            
            if(get_spi_busy_status() == SPI_TRANSFER_BUSY)
            {
                e_transfer_status = SECOND_SEND_STATUS;
            }
            else if(get_spi_busy_status() == SPI_TRANSFER_IDLE)//等待接收完成
            {
                g_transfer.read_len = spi_read_data(g_transfer.read_data,g_transfer.send_len);
                if( g_transfer.read_data[0] >= 2 && 
                    g_transfer.read_data[0] <=TRANSFER_SEND_SIZE)
                {
                    if (g_transfer.read_len > g_transfer.read_data[0])
                        g_transfer.remainLength = g_transfer.read_len - 1;
                    else
                        g_transfer.remainLength = g_transfer.read_data[0] - 1;
                    if (g_transfer.remainLength != 0)
                    {
                        spi_status = spi_send(&g_transfer.send_data[1],g_transfer.remainLength);
                        if(spi_status == SPI_INVALID_LENGTH)
                        {
                            break;
                        }
                        if(spi_status == SPI_SEND_BUSY) //等待再次发送
                        {
                            e_transfer_status = SECOND_SEND_STATUS;
                        }
                        if(spi_status == SPI_SEND_SUCCESS)
                        {
                            e_transfer_status = RECEIVE_STATUS;
                        }
                    }
                    else //失败进入失败状态
                    {
                       e_transfer_status = FAIL_STATUS; 
                    }
                }
                else//失败进入失败状态
                {
                    e_transfer_status = FAIL_STATUS; 
                }
            }
            
        break;
            
        case RECEIVE_STATUS:
            
            if(get_spi_busy_status() == SPI_TRANSFER_BUSY)
            {
                e_transfer_status = RECEIVE_STATUS;
                break;
            }
            else if(get_spi_busy_status() == SPI_TRANSFER_IDLE)//等待接收完成
            {
                g_transfer.read_len = spi_read_data(&g_transfer.read_data[1],g_transfer.remainLength);
                rcvLength = g_transfer.read_data[0];
                if ( rcvLength >= 2
                    && rcvLength <= MTI_PACKAGE_MAX_DATA
                    && checksum (g_transfer.read_data, rcvLength-1) == g_transfer.read_data[rcvLength-1])
                {
                    g_transfer.get_len = g_transfer.read_data[0]-2;
                    memcpy(g_transfer.get_data,&g_transfer.read_data[1],g_transfer.get_len);
                    e_transfer_status = END_STATUS;
                    break;
                }
                else // 失败
                {
                    e_transfer_status = FAIL_STATUS;
                }
            }

        break;
            
        case FAIL_STATUS:
            
            spi_cs_control(DISABLE);         //拉高CS管脚
            e_transfer_status = HOLD_STATUS;  //从新开始传输
            Timer_Set(&status_timer,2000);  
            g_transfer.retry ++;
            if(g_transfer.retry == MTI_TRANSFER_MAX_RETRY)
            {
                g_transfer.get_len = 0;
                e_transfer_status = END_STATUS;
                break;
            }
            
        break;
            
        case END_STATUS:
            
            spi_cs_control(DISABLE); //拉高CS管脚
            e_transfer_status = SUCESS_STATUS;
            Timer_Set(&status_timer,2000);
            
        break;
        
        default:
        break;
    }
}


/*************************************************
 * Function:    transfer_send
 * Description: 申请发送数据
 * Input:       send_buf - 要发送的数据的缓冲区
 *              size - 要发送的数据长度
 * Output:      无
 * Return:      TRANSFER_STATUS_T -发送是否成功
 *              若为 TRANSFER_SUCCESS，代表发送成功
 *              若为 TRANSFER_BUSY，代表之前的传输尚未结束，应等待空闲后再次尝试发送
 *              若为 TRANSFER_INVALID_LENGTH，代表申请发送的数据长度过长，不用再次尝试
 * Others:      无
*************************************************/
TRANSFER_STATUS_T    transfer_send (const uint8_t *send_buf, uint16_t size)
{
    if(size > MTI_PDU_MAX_DATA)
    {
        return TRANSFER_INVALID_LENGTH;
    }
    if(e_transfer_status != EMPTY_STATUS) //如果当前状态为空时在传输
    {
        return TRANSFER_BUSY;
    }
    g_transfer.send_data[0] = size+2;
    if(size)
    {
        memcpy(&g_transfer.send_data[1],send_buf,size);
    }
    
    g_transfer.send_data[size+1] = checksum (g_transfer.send_data, size+1);
    g_transfer.send_len = size+2;
    e_transfer_status = START_STATUS;//进入开始传输状态
    
    return  TRANSFER_SUCCESS;
}
/*************************************************
 * Function:    set_transfer_status(bool status)
 * Description: 设置传输的标志
 * Input:       TRANSFER_BUSY/TRANSFER_IDLY
 * Output:      无
 * Return: 
 * Others:      无
*************************************************/
static void set_transfer_status(bool status)
{
    g_transfer_busy = status; 
}
/*************************************************
 * Function:    transfer_is_busy
 * Description: 检查传输是否处于忙碌状态
 * Input:       无
 * Output:      无
 * Return:      true 代表当前传输尚未结束
 *              false 代表传输结束，可以读取之前传输的结果，或启动新的传输
 * Others:      无
*************************************************/
bool    transfer_is_busy (void)
{
    return  g_transfer_busy;
}


/*************************************************
 * Function:    transfer_read
 * Description: 读取传输结果
 * Input:       read_buf - 返回的传输结果缓冲区
 *              read_buf_size - 结果缓冲区长度
 * Output:      无
 * Return:      传输结果实际长度
 * Others:      应在传输完毕后读取传输结果，使用 tansfer_is_busy 判断传输是否结束；
 *              若在传输尚未结束时读取，则返回的传输实际长度为 0
*************************************************/
uint16_t transfer_read (uint8_t *read_buf, uint16_t read_buf_size)
{
    if(transfer_is_busy() == TRANSFER_BUSY_STATUS)
    {
        return g_transfer.get_len;
    }
    if(g_transfer.get_len > read_buf_size)
    {
        return g_transfer.get_len;
    }
    memcpy(read_buf,g_transfer.get_data,read_buf_size);
    return g_transfer.get_len;
}

/*********************************************************************
 * @fn      checksum
 *
 * @brief   计算数据校验和.
 *
 * @param   pdat - 等待进行校验和处理的数据缓冲区.
 * @param   size - 等待进行校验和处理的数据长度.
 *
 * @return  校验和计算结果.
 */
static uint8_t	checksum (uint8_t *pdat, uint8_t size)
{
	uint8_t sum = 0;
	while (size--)
		sum += *pdat++;
	return	sum;
}


