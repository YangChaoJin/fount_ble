#include <stdio.h>
#include <string.h>
#include "apps_program.h"
#include "gatt_uuid.h"
#include "gp_timer.h"
#include "app_state.h"
#include "SDK_EVAL_Config.h"
#include "connection_service.h"
#include "bluenrg1_stack.h"
#include "ble_mutually.h"
#include "ble_status.h"
#include "fifo.h"
#include "fount_spi.h"
/**************************静态函数************************************/

/**************************回调函数************************************/
static uint8_t  auth_process(auth_Struct *auth_status,index_handle tab_handle);
/**************************静态变量************************************/
static queue_msg s_queue_msg;
/**************************全局变量************************************/
Bt_handle_t g_getHandle;       //characteristic handle
connet_event g_connet_event_flag;//手机连接事件
system_event g_system_event;    //系统事件
spi_status e_spi_status;
circular_fifo_t spi_tx_fifo;
uint8_t spi_queue_buffer[20];
/**************************外部变量************************************/
typedef struct timer system_timer;
extern system_timer system_clock,auth_timer[MAX_NUM_BLE_CONNS]; //1S定时器用于两次广播
typedef struct timer event_timer; //
event_timer event_clock;
struct timer spi_proc_timer;
extern uint8_t g_dma_rv_flag;
extern  uint8_t spi_buffer_rx[SPI_BUFF_SIZE];

void spi_queue_init(void)
{
    fifo_init(&spi_tx_fifo, SPI_BUFF_SIZE, spi_queue_buffer, 0x2);
}
 /*************************************************
*	Function: void user_task(void)
*	Description: 用户任务
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
**************************************************/
void user_task(void)
{
    uint8_t status=0;
    g_getHandle = get_att_handle();
    
   // auth_process(g_auth_state,g_index_handle); //认证判断
	switch(g_system_event.system_event)  //广播
	{
        case app_data_proc_event:
            if( mti_queue_rev( &s_queue_msg.size, s_queue_msg.buffer) ) //收到数据
            {      
                status = fifo_put(&spi_tx_fifo, s_queue_msg.size, s_queue_msg.buffer);
                //status =mti_transfer (s_queue_msg.size, s_queue_msg.buffer);
                if(status == FAILURE) //失败
                {
                    
                }
                
            }
            if(g_dma_rv_flag)
            {
               g_dma_rv_flag = 0;
               system_event_busy_clear(); //清楚忙标志 
               system_event_set(dma_data_proc_event); 
            }
            break;
        case dma_data_proc_event:


            
            break;
        case null_event:
            
            break;
		default:
            
			break;
		
	 }
}
void spi_data_proc(void)
{   

    switch(e_spi_status)
    {
        case star_event:
            Timer_Set(&spi_proc_timer, 2);
           if(Timer_Expired(&spi_proc_timer))
           {
               e_spi_status = proc_event;
           }
        break;
        case proc_event:

        break;
        case end_event:
            if(Timer_Expired(&spi_proc_timer))
            {

            }
        break;
        case spi_null:
            
        break;   
    }
    
}
uint8_t multi_connection(void)
{
    uint8_t ret =0;
    if( (g_connet_event_flag.connet_num  == 0x01) && 
            ( Timer_Expired(&system_clock) ) && get_connet_flag(CONNNET_EVENT) )
    { 
        ret = creat_connection();
        if (ret != STATUS_SUCCESS) 
        {
            return STATUS_ERR;
        }

    }
    return STATUS_SUCCESS; 
}
 /*************************************************
*	Function: void set_connet_num_flag((uint8_t value))
*	Description: 设置连接的数量
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
**************************************************/
static uint8_t  auth_process(auth_Struct *auth_status,
                index_handle tab_handle)
{   
    uint8_t status;
    uint8_t index=0;
    for(index=0;index<MAX_NUM_BLE_CONNS;index++)
    {
        if(tab_handle.connet_flag[index] == 1)//都得到序号是哪个要做认证
        {
            if(tab_handle.connet_flag[index] == 1 && ( Timer_Expired(&auth_timer[index]) )) 
            {
                if(auth_status[index].is_Authenticated == TRUE)
                {
                   status =0; 
                }
                else   //认证失败后断开连接
                {
                   Timer_Set(&auth_timer[index], 1000); //10S后判断是否认证通过
                   aci_gap_terminate(tab_handle.connet_handle[index],0x13); 
                }
            } 
            
        } 
    }
    return  status;
}
 /*************************************************
*	Function: void set_connet_num_flag((uint8_t value))
*	Description: 设置连接的数量
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
**************************************************/
void set_connet_num_flag(uint8_t value)
{
    
    g_connet_event_flag.connet_num  =  value;      
    
}
 /*************************************************
*	Function: uint8_t connet_flag_status((uint8_t flag))
*	Description: 设置连接的状态
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
**************************************************/
uint8_t  get_connet_flag(uint8_t flag) 
{
    return (g_connet_event_flag.connet_flag & flag);
}
void connet_event_flag_set(uint8_t flag) 
{
    g_connet_event_flag.connet_flag |= flag;
}
void connet_event_flag_clear(uint8_t flag) 
{
    g_connet_event_flag.connet_flag &= ~flag;
}
 /*************************************************
*	Function: uint8_t connet_flag_status((uint8_t flag))
*	Description: 设置连接的状态 如果当前系统处于忙的话，
交给定时器区处理
*	Input: system_status 事件值
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
**************************************************/
void system_event_set(system_status value)
{   
    if(g_system_event.busy) //判断当前是否在忙
    {
        Timer_Set(&event_clock, 50);//启动定时器
        g_system_event.clock  = ENABLE;//时钟标准使能
        g_system_event.cache_event =  value; //把值写入缓存区
    }
    else
    {
        g_system_event.system_event = value;
        system_event_busy_set();
    }
}
 /*************************************************
*	Function: toggle_state
*	Description:定时器处理当前忙的事件 50MS强制切换
*	Input: system_status 事件值
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
**************************************************/
uint8_t toggle_state(void)
{
    if(g_system_event.clock  == ENABLE)
    {
        if( Timer_Expired(&system_clock) && g_system_event.busy == DISABLE )
        {
            g_system_event.clock =DISABLE;
            g_system_event.system_event = g_system_event.cache_event;//将缓存区写入
            system_event_busy_set();
            return 0;
        }
        
    }
    return 0;
}
void system_event_busy_set(void) 
{
    g_system_event.busy = ENABLE;
}
void system_event_busy_clear(void) 
{
    g_system_event.busy =DISABLE;
}


















