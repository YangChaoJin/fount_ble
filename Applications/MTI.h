/*************************************************
Copyright (C), 2018-2018, Ecomo Tech. Co., Ltd.
File name:  MTI.h
Author: 王明浩        Version: V1.0       Date: 2018-12-17
Description:    与MSP430的交互接口，MTI是 mcu transfer interface 的简称
Others:
Function List:  // 主要函数列表，每条记录应包括函数名及功能简要说明
    1. mti_init 初始化传输管理接口
    2. mti_tick 状态机切换工作入口
    3. mti_send_address 发送蓝牙模块本身 MAC 地址
    4. mti_request_version 发送获取版本号的请求
    5. mti_request_connection_cnt 发送获取最大连接数量的请求
    6. mti_request_reboot_self 发送申请重启的请求
    7. mti_send_connect_event 发送蓝牙连接建立事件
    8. mti_send_disconnect_event 发送蓝牙连接断开事件
    9. mti_send_indicate_confirm_event 发送接收到 indicate 对应的 confirm 事件
    10. mti_send_request_data 发送接收到通道数据 事件
    11. mti_receive_version_event 接收到版本信息时的处理函数
    12. mti_receive_connection_cnt_event 接收到最大连接数量的处理函数
    13. mti_receive_device_info_event 接收到设置设备信息时的处理函数
    14. mti_receive_request_disconnect_event 接收到请求断开连接时的处理函数
    15. mti_receive_response_data_event 接收到通道数据时的处理函数

History:
    1.  Version: V1.0
        Date:2018-12-17
        Author: 王明浩
        Modification: 初次编码
*************************************************/


#ifndef     __MTI_H__
#define     __MTI_H__


#include <stdint.h>


/*  定义通道数据的最大长度               */
#define     MTI_BLE_DATA_SIZE   (20)





/*  申请发送数据时的返回状态    */
typedef enum
{
    MTI_SEND_SUCCESS,           /*  操作成功    */
    MTI_SEND_BUFFER_FULL,       /*  发送缓冲区已满，操作失败*/
    MTI_SEND_INVALID_LENGTH,    /*  发送的数据长度非法 */
} MTI_SEND_STATUS_T;


/*  接收到数据时的处理结果状态 */
typedef enum
{
    MTI_PROCESS_SUCCESS,        /*  处理成功 */
    MTI_PROCESS_LATER,          /*  处理失败，需要稍后重试   */
    MTI_PROCESS_IGNORE          /*  处理失败，可以忽略该数据    */
} MTI_PROCESS_STATUS_T;




/*************************************************
 * Function:    mti_init
 * Description: 初始化传输管理接口
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    mti_init (void);


/*************************************************
 * Function:    mti_tick
 * Description: 状态机切换工作入口
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    mti_tick (void);



/*************************************************
 * Function:    mti_send_address
 * Description: 发送蓝牙模块本身 MAC 地址
 * Input:       mac_address - 蓝牙模块地址，长度固定6个字节
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_send_address (const uint8_t *mac_address);


/*************************************************
 * Function:    mti_request_version
 * Description: 发送获取版本号的请求
 * Input:       无
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_request_version (void);


/*************************************************
 * Function:    mti_request_connection_cnt
 * Description: 发送获取最大连接数量的请求
 * Input:       无
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_request_connection_cnt (void);



/*************************************************
 * Function:    mti_request_reboot_self
 * Description: 发送申请重启的请求
 * Input:       无
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_request_reboot_self (void);



/*************************************************
 * Function:    mti_send_connect_event
 * Description: 发送蓝牙连接建立事件，应在连接认证通过后再发送到 MSP430
 * Input:       conn_handle - 连接的句柄
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_connect_event (uint16_t conn_handle);


/*************************************************
 * Function:    mti_send_disconnect_event
 * Description: 发送蓝牙连接断开事件，只有认证通过的连接才需要通知MSP430
 * Input:       conn_handle - 连接的句柄
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_disconnect_event (uint16_t conn_handle);


/*************************************************
 * Function:    mti_send_indicate_confirm_event
 * Description: 发送接收到 indicate 对应的 confirm 事件
 * Input:       conn_handle - 连接的句柄
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_indicate_confirm_event (uint16_t conn_handle);


/*************************************************
 * Function:    mti_send_request_data
 * Description: 发送接收到通道数据 事件
 * Input:       conn_handle - 连接句柄
 *              data_buf - 通道数据缓冲区
 *              length - 通道数据长度
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 *              MTI_SEND_INVALID_LENGTH - 数据长度非法
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_request_data (uint16_t conn_handle,
                                                    const uint8_t *data_buf, 
                                                    uint8_t length);




/*************************************************
 * Function:    mti_receive_version_event
 * Description: 接收到版本信息时的处理函数
 * Input:       version_buf - 版本信息缓冲区
 *              length - 版本信息长度
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
*************************************************/
MTI_PROCESS_STATUS_T    mti_receive_version_event (const uint8_t *version_buf, uint8_t length);


/*************************************************
 * Function:    mti_receive_connection_cnt_event
 * Description: 接收到最大连接数量的处理函数
 * Input:       connection_cnt - 最大连接数量
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
MTI_PROCESS_STATUS_T    mti_receive_connection_cnt_event (uint8_t connection_cnt);


/*************************************************
 * Function:    mti_receive_device_info_event
 * Description: 接收到设置设备信息时的处理函数
 * Input:       id - 设备信息代码
 *              info_buf - 设备信息缓冲区
 *              length - 设备信息长度
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
MTI_PROCESS_STATUS_T    mti_receive_device_info_event (uint8_t id, const uint8_t *info_buf, uint8_t length);



/*************************************************
 * Function:    mti_receive_device_info_event
 * Description: 接收到请求断开连接时的处理函数
 * Input:       conn_handle - 连接句柄
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
MTI_PROCESS_STATUS_T    mti_receive_request_disconnect_event (uint16_t conn_handle);


/*************************************************
 * Function:    mti_receive_response_data_event
 * Description: 接收到通道数据时的处理函数
 * Input:       conn_handle - 连接句柄
 *              data_buf - 数据缓冲区
 *              length - 数据长度
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
MTI_PROCESS_STATUS_T    mti_receive_response_data_event (uint16_t conn_handle,
                                                    const uint8_t *data_buf, uint8_t length);



#endif      /*  end of __MTI_H__*/

