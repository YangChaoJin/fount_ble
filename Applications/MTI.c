/*************************************************
Copyright (C), 2018-2018, Ecomo Tech. Co., Ltd.
File name:  MTI.c
Author: 王明浩        Version: V1.0       Date: 2018-12-17
Description:    与MSP430的交互接口，MTI是 mcu transfer interface 的简称
Others:
Function List:  // 主要函数列表，每条记录应包括函数名及功能简要说明
    1. mti_init 初始化传输管理接口
    2. mti_tick 状态机切换工作入口
    3. mti_send_address 发送蓝牙模块本身 MAC 地址
    4. mti_request_version 发送获取版本号的请求
    5. mti_request_connection_cnt 发送获取最大连接数量的请求
    6. mti_request_reboot_self 发送申请重启的请求
    7. mti_send_connect_event 发送蓝牙连接建立事件
    8. mti_send_disconnect_event 发送蓝牙连接断开事件
    9. mti_send_indicate_confirm_event 发送接收到 indicate 对应的 confirm 事件
    10. mti_send_request_data 发送接收到通道数据 事件
    11. mti_receive_version_event 接收到版本信息时的处理函数
    12. mti_receive_connection_cnt_event 接收到最大连接数量的处理函数
    13. mti_receive_device_info_event 接收到设置设备信息时的处理函数
    14. mti_receive_request_disconnect_event 接收到请求断开连接时的处理函数
    15. mti_receive_response_data_event 接收到通道数据时的处理函数


History:
    1.  Version: V1.0
        Date:2018-12-17
        Author: 王明浩
        Modification: 初次编码
*************************************************/



#include "MTI.h"
#include "transfer.h"
#include "fifo.h"
#include "gp_timer.h"
#include "osal.h"
#include <assert.h>


/*  截取一个16位数据的高8位           */
#define HI_UINT16(a) (((a) >> 8) & 0xFF)

/*  截取一个16位数据的低8位           */
#define LO_UINT16(a) ((a) & 0xFF)

/*  将2个8位数据拼合成一个16位数据 */
#define BUILD_UINT16(loByte, hiByte) \
          ((uint16_t)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))





/*  定义 MTI 传输管理层的缓冲区长度  
 *  传输管理层需要附加1个字节的通道代码和2个字节的连接句柄，共需要额外的3个字节
 */
#define MTI_BUFFER_DATA_SIZE    (MTI_BLE_DATA_SIZE + 3)


/*  发送队列中的最大信息个数
 */
#define MTI_FIFO_ITEM_SIZE      (100)


/*  发送队列的缓冲区数据长度
 * 该长度为名义上的长度，用于初始化 fifo_init 时的参数
 */
#define MTI_FIFO_BUFFER_SIZE    ((MTI_FIFO_ITEM_SIZE) * (MTI_BUFFER_DATA_SIZE))


/* 发送缓冲区的实际数据长度
 * 该长度用于申请或声明数据
 */
#define MTI_FIFO_REAL_BUFFER_SIZE   ((MTI_FIFO_BUFFER_SIZE) + (MTI_BUFFER_DATA_SIZE) - 1)


/* 发送的延时时间
 */
#define MTI_MIN_WAIT_TICKS      (1)     /*  最小等待间隔      */
#define MTI_MAX_WAIT_TICKS      (50)    /*  最大等待间隔      */





/*  MTI 的状态机定义      */
typedef enum
{
    MTI_STATE_INIT,     /*  状态机处于初始状态            */
    MTI_STATE_IDLE,     /*  状态机处于空闲状态         */
    MTI_STATE_SEND,     /*  状态机处于发送状态 */
    MTI_STATE_RECEIVE,  /*  状态机处于接收状态 */
    MTI_STATE_PROCESS   /*  状态机处于处理状态 */
} MTI_STATE_T;



/*  MTI 管理器   */
typedef struct
{
    MTI_STATE_T state;                  /*  状态机当前状态       */
    circular_fifo_t send_fifo_struct;   /*  发送缓冲队列      */
    uint8_t send_fifo_buffer[MTI_FIFO_REAL_BUFFER_SIZE];    /*  发送缓冲队列的数据缓冲区*/
    struct timer wait_timer;            /*  等待时钟    */
    uint16_t    wait_period_in_ticks;      /*  以TICK为单位的等待时间           */
    uint8_t cur_send_buffer[MTI_BUFFER_DATA_SIZE];  /*  当前发送数据缓冲区        */
    uint8_t cur_send_length;                        /*  当前发送数据长度         */
    uint8_t cur_rcv_buffer[MTI_BUFFER_DATA_SIZE];   /*  当前接收数据缓冲区         */
    uint8_t cur_rcv_length;                         /*  当前接收数据长度          */
} MTI_MANAGER_T;



/*  传输通道定义      */
typedef enum
{
    MTI_CHANNEL_REQUEST = 0,    /*  数据请求通道，用于传输手机发送到蓝牙的数据 */
    MTI_CHANNEL_RESPONSE = 1,   /*  数据回复通道，用于传输蓝牙发送到手机的数据 */
    MTI_CHANNEL_SYSTEM = 0xff   /*  系统信息通道，用于蓝牙和MCU交换控制信息                   */
} MTI_CHANNEL_T;


/*  系统信息通道的数据类型代码定义               */
typedef enum
{
    MTI_SYSTEM_SEND_ADDRESS = 0,
    MTI_SYSTEM_VERSION = 1,
    MTI_SYSTEM_REBOOT = 2,
    MTI_SYSTEM_CONNECT = 3,
    MTI_SYSTEM_DISCONNECT = 4,
    MTI_SYSTEM_CONFIRM = 5,
    MTI_SYSTEM_MAX_CONNS = 6,
    MTI_SYSTEM_DEVICE_INFO = 7   
} MTI_SYSTEM_ID_T;





/* MTI 控制管理器
 * 几乎所有函数都需要访问控制器
 */
static MTI_MANAGER_T s_mti_manager;



/*************************************************
 * Function:    mti_queue_msg
 * Description: 向 FIFO 存储信息
 * Input:       msg_buf - 信息缓冲区
 *              length - 信息长度
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 存储成功
 *              MTI_SEND_BUFFER_FULL - 缓冲区已满，发送失败
 * Others:      无
*************************************************/
static MTI_SEND_STATUS_T mti_queue_msg (const uint8_t *msg_buf, uint8_t length)
{
    uint8_t ret = fifo_put_var_len_item (&s_mti_manager.send_fifo_struct, length, msg_buf);

    if (0 == ret)
    {
        return  MTI_SEND_SUCCESS;
    }
    else
    {
        return  MTI_SEND_BUFFER_FULL;
    }
}


/*************************************************
 * Function:    mti_refresh_send_msg
 * Description: 从 FIFO 读取数据，刷新待发送数据
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
static void mti_refresh_send_msg (void)
{
    uint16_t msg_len;

    /*  从 FIFO 获取数据       */
    uint8_t ret = fifo_get_var_len_item (&s_mti_manager.send_fifo_struct, 
                                        &msg_len, 
                                        s_mti_manager.cur_send_buffer);    
                                        
    /*  检查数据是否获取成功          */
    if (0 == ret)
    {
        assert (msg_len <= MTI_BUFFER_DATA_SIZE);
        s_mti_manager.cur_send_length = (uint8_t)msg_len;
    } 
    else
    {
        s_mti_manager.cur_send_length = 0;
    }    
}


/*************************************************
 * Function:    mti_process_channel_system
 * Description: 处理接收到的系统通道数据，数据在 s_mti_manager 中保存
 * Input:       无
 * Output:      无
 * Return:      MTI_PROCESS_SUCCESS - 处理成功
 *              MTI_PROCESS_LATER - 处理失败，需要稍后重试
 *              MTI_PROCESS_IGNORE - 处理失败，可以忽略该数据
 * Others:      无
*************************************************/
static MTI_PROCESS_STATUS_T mti_process_channel_system (void)
{
    MTI_SYSTEM_ID_T system_id;
    MTI_PROCESS_STATUS_T process_status = MTI_PROCESS_IGNORE;    

    /*  获取系统功能代码        */
    if (s_mti_manager.cur_rcv_length < 2)
    {
        return  MTI_PROCESS_IGNORE;
    }
    system_id = (MTI_SYSTEM_ID_T)s_mti_manager.cur_rcv_buffer[1];

    /*  根据系统功能代码，决定数据处理方法 */
    switch (system_id)
    {
        case MTI_SYSTEM_VERSION:
        {   
            /*  获取版本信息      */
            uint8_t *version_buf = &s_mti_manager.cur_rcv_buffer[2];
            uint8_t version_length = s_mti_manager.cur_rcv_length - 2;
        
            /* 调用版本信息处理 */
            process_status = mti_receive_version_event (version_buf, version_length);
            break;
        }
        
        case MTI_SYSTEM_DISCONNECT:
        {            
            /*  检查长度        */
            if (4 != s_mti_manager.cur_rcv_length)
            {
                process_status = MTI_PROCESS_IGNORE;
            }
            else
            {
                uint16_t conn_handle;

                /*  构建连接句柄      */
                conn_handle = BUILD_UINT16 (s_mti_manager.cur_rcv_buffer[2], 
                                            s_mti_manager.cur_rcv_buffer[3]);

                /*  处理连接断开请求        */
                process_status = mti_receive_request_disconnect_event (conn_handle);
            }
            break;
        }
        
        case MTI_SYSTEM_MAX_CONNS:
        {
            /*  检查长度    */
            if (3 != s_mti_manager.cur_rcv_length)
            {
                process_status = MTI_PROCESS_IGNORE;
            }
            else
            {
                /*  提取最大连接数量        */
                uint8_t max_conn = s_mti_manager.cur_rcv_buffer[2];

                /*  处理最大连接数事件 */
                process_status = mti_receive_connection_cnt_event (max_conn);
            }
            break;
        }
        
        case MTI_SYSTEM_DEVICE_INFO:
        {
            /*  检查长度    */
            if (s_mti_manager.cur_rcv_length < (2 + 1))
            {
                process_status = MTI_PROCESS_IGNORE;
            }
            else
            {
                /*  提取信息    */
                uint8_t device_info_id = s_mti_manager.cur_rcv_buffer[2];
                uint8_t *device_info_buffer = &s_mti_manager.cur_rcv_buffer[3];
                uint8_t device_info_length = s_mti_manager.cur_rcv_length - 3;
                
                /*  处理设备信息事件    */
                process_status = mti_receive_device_info_event (device_info_id, 
                                        device_info_buffer, device_info_length);
            }
            break;
        }
        
        default:
            process_status = MTI_PROCESS_IGNORE;
            break;
    }
    
    return  process_status;
}


/*************************************************
 * Function:    mti_process_channel_response
 * Description: 处理接收到的回复通道数据，数据在 s_mti_manager 中保存
 * Input:       无
 * Output:      无
 * Return:      MTI_PROCESS_SUCCESS - 处理成功
 *              MTI_PROCESS_LATER - 处理失败，需要稍后重试
 *              MTI_PROCESS_IGNORE - 处理失败，可以忽略该数据
 * Others:      无
*************************************************/
static MTI_PROCESS_STATUS_T mti_process_channel_response (void)
{
    /*  回复通道的数据格式：
     *  byte_idx     含义
     *  0           通道码，此处已固定为 MTI_CHANNEL_RESPONSE
     *  1           连接句柄低8位
     *  2           连接句柄高8位
     *  3-n         回复的数据，应至少包含1个字节
     */

    MTI_PROCESS_STATUS_T process_status = MTI_PROCESS_IGNORE;

    /*  检查长度    */
    if (s_mti_manager.cur_rcv_length < 4)
    {
        process_status = MTI_PROCESS_IGNORE;
    }
    else
    {
        /*  提取信息    */
        uint16_t conn_handle = BUILD_UINT16 (s_mti_manager.cur_rcv_buffer[1], 
                                            s_mti_manager.cur_rcv_buffer[2]);
        uint8_t *msg_buffer = &s_mti_manager.cur_rcv_buffer[3];
        uint8_t msg_length = s_mti_manager.cur_rcv_length - 3;

        /*  处理数据    */
        process_status = mti_receive_response_data_event (conn_handle, msg_buffer, msg_length);
    }

    return  process_status;
}




/*************************************************
 * Function:    mti_process_rcv_data
 * Description: 处理接收到的数据，数据在 s_mti_manager 中保存
 * Input:       无
 * Output:      无
 * Return:      MTI_PROCESS_SUCCESS - 处理成功
 *              MTI_PROCESS_LATER - 处理失败，需要稍后重试
 *              MTI_PROCESS_IGNORE - 处理失败，可以忽略该数据
 * Others:      无
*************************************************/
static  MTI_PROCESS_STATUS_T    mti_process_rcv_data (void)
{
    uint8_t channel;
    MTI_PROCESS_STATUS_T process_status = MTI_PROCESS_IGNORE;
    
    /*  检查数据长度      */
    if (0 == s_mti_manager.cur_rcv_length)
    {
        return  MTI_PROCESS_IGNORE;
    }

    /*  根据通道代码分发数据处理方 */
    channel = s_mti_manager.cur_rcv_buffer[0];
    switch (channel)
    {    
        /*  请求通道应该是 蓝牙模块发往MCU的数据通道代码，
         *  蓝牙不对MCU异常返回的数据做处理
         */
        case MTI_CHANNEL_REQUEST:
            process_status = MTI_PROCESS_IGNORE;
            break;

        case MTI_CHANNEL_RESPONSE:
            process_status = mti_process_channel_response ();
            break;
            
        case MTI_CHANNEL_SYSTEM:
            process_status = mti_process_channel_system ();
            break;

        /*  未知通道，不进行处理*/
        default:
            process_status = MTI_PROCESS_IGNORE;
            break;
    }

   return   process_status;
}



/*************************************************
 * Function:    mti_init
 * Description: 初始化传输管理接口
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    mti_init (void)
{
    /*  初始化传输层  */
    transfer_init ();

    /*  初始化控制器      */
    s_mti_manager.state = MTI_STATE_INIT;
    s_mti_manager.cur_rcv_length = 0;
    fifo_init (&s_mti_manager.send_fifo_struct,     /*  fifo 管理结构体    */
                MTI_FIFO_BUFFER_SIZE,               /*  fifo 缓冲区长度    */
                s_mti_manager.send_fifo_buffer,    /*  fifo 缓冲区      */
                2);                                 /*  fifo 对齐个数       */
}


/*************************************************
 * Function:    mti_tick
 * Description: 状态机切换工作入口
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    mti_tick (void)
{
    /*  刷新传输层的状态机    */
    transfer_tick ();

    /*  状态切换    */
    switch (s_mti_manager.state)
    {
        /*  初次启动，初始化定时器           */
        case MTI_STATE_INIT:
        {
            s_mti_manager.wait_period_in_ticks = MTI_MIN_WAIT_TICKS;
            Timer_Set (&s_mti_manager.wait_timer, s_mti_manager.wait_period_in_ticks);
            s_mti_manager.state = MTI_STATE_IDLE;
            break;
        }

        /*  空闲状态，等待发送数据       */
        case MTI_STATE_IDLE:
        {
            /*  定时器是否超时       */
            bool is_timeout = Timer_Expired (&s_mti_manager.wait_timer);

            /*  刷新待发送数据           */
            mti_refresh_send_msg ();

            /*  若定时器超时，或有待发送数据，则进入发送状态              */
            if (is_timeout 
                || (0 != s_mti_manager.cur_send_length))
            {
                s_mti_manager.state = MTI_STATE_SEND;
            }

            break;
        }

        /*  尝试发送数据      */
        case MTI_STATE_SEND:
        {
            /*  尝试发送数据      */
            TRANSFER_STATUS_T status = transfer_send (s_mti_manager.cur_send_buffer, s_mti_manager.cur_send_length);

            /*  根据发送结果，决定下一步状态              */
            if (TRANSFER_SUCCESS == status)
            {
                /*  若发送成功，则进入等待接收状态           */
                s_mti_manager.state = MTI_STATE_RECEIVE;
            }
            else if (TRANSFER_INVALID_LENGTH == status)
            {
                /*  正常情况下不应该有长度异常，因为在对外接口处已经拦截了非法数据长度。
                 *  若在发送时遇到长度异常，应检查代码。
                 */
                while (1);
            } 
            else if (TRANSFER_BUSY == status)
            {
                /*  底层传输处于忙碌状态，稍后重试，状态不需要改变               */
            }
            else
            {
                /*  未定义的状态，应检查代码            */
                while (1);
            }

            break;
        }

        /*  等待数据发送完毕            */
        case MTI_STATE_RECEIVE:
        {
            bool is_busy = transfer_is_busy ();
            if (!is_busy)
            {
                uint16_t real_length = transfer_read (s_mti_manager.cur_rcv_buffer, MTI_BUFFER_DATA_SIZE);

                if (real_length > MTI_BUFFER_DATA_SIZE)
                {
                    /*  数据长度不应大于缓冲区长度 */
                    while (1);
                }
                else
                {
                    s_mti_manager.cur_rcv_length = (uint8_t)real_length;
                    s_mti_manager.state = MTI_STATE_PROCESS;
                }
            }

            break;
        }

        /*  处理分析接收到的数据          */
        case    MTI_STATE_PROCESS:
        {
            /*  处理接收到的数据 */
            MTI_PROCESS_STATUS_T process_status = mti_process_rcv_data ();
            assert ((MTI_PROCESS_SUCCESS == process_status)
                    || (MTI_PROCESS_LATER == process_status)
                    || (MTI_PROCESS_IGNORE == process_status));

            /*  若需要稍后重试，则状态不变 */
            if (MTI_PROCESS_LATER == process_status)
            {
                break;
            }           

            /*  计算新的延时时间        */
            if (0 == s_mti_manager.cur_send_length
                && 0 == s_mti_manager.cur_rcv_length)
            {
                if (s_mti_manager.wait_period_in_ticks < MTI_MAX_WAIT_TICKS)
                {
                    s_mti_manager.wait_period_in_ticks++;
                }
            } 
            else
            {
                s_mti_manager.wait_period_in_ticks = MTI_MIN_WAIT_TICKS;
            }

            /*  启动定时器，切换状态          */
            Timer_Set (&s_mti_manager.wait_timer, s_mti_manager.wait_period_in_ticks);
            s_mti_manager.state = MTI_STATE_IDLE;
            break;
        }

        default:
        {
            /*  异常状态，不应该进入未定义状态   */
            while (1);
        }
        
    }

}



/*************************************************
 * Function:    mti_send_address
 * Description: 发送蓝牙模块本身 MAC 地址
 * Input:       mac_address - 蓝牙模块地址，长度固定6个字节
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_send_address (const uint8_t *mac_address)
{
    uint8_t buffer[2+6];

    buffer[0] = MTI_CHANNEL_SYSTEM;
    buffer[1] = MTI_SYSTEM_SEND_ADDRESS;
    Osal_MemCpy (&buffer[2], mac_address, 6);

    return  mti_queue_msg (buffer, sizeof(buffer));
}


/*************************************************
 * Function:    mti_request_version
 * Description: 发送获取版本号的请求
 * Input:       无
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_request_version (void)
{
    uint8_t buffer[2];

    buffer[0] = MTI_CHANNEL_SYSTEM;
    buffer[1] = MTI_SYSTEM_VERSION;

    return  mti_queue_msg (buffer, sizeof(buffer));
}



/*************************************************
 * Function:    mti_request_connection_cnt
 * Description: 发送获取最大连接数量的请求
 * Input:       无
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_request_connection_cnt (void)
{
    uint8_t buffer[2];

    buffer[0] = MTI_CHANNEL_SYSTEM;
    buffer[1] = MTI_SYSTEM_MAX_CONNS;

    return  mti_queue_msg (buffer, sizeof(buffer));
}




/*************************************************
 * Function:    mti_request_reboot_self
 * Description: 发送申请重启的请求
 * Input:       无
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T   mti_request_reboot_self (void)
{
    uint8_t buffer [2];

    buffer[0] = MTI_CHANNEL_SYSTEM;
    buffer[1] = MTI_SYSTEM_REBOOT;

    return  mti_queue_msg (buffer, sizeof(buffer));
}



/*************************************************
 * Function:    mti_send_connect_event
 * Description: 发送蓝牙连接建立事件，应在连接认证通过后再发送到 MSP430
 * Input:       conn_handle - 连接的句柄
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_connect_event (uint16_t conn_handle)
{
    uint8_t buffer [4];

    buffer[0] = MTI_CHANNEL_SYSTEM;
    buffer[1] = MTI_SYSTEM_CONNECT;
    buffer[2] = LO_UINT16(conn_handle);
    buffer[3] = HI_UINT16(conn_handle);

    return  mti_queue_msg (buffer, sizeof(buffer));
}



/*************************************************
 * Function:    mti_send_disconnect_event
 * Description: 发送蓝牙连接断开事件，只有认证通过的连接才需要通知MSP430
 * Input:       conn_handle - 连接的句柄
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_disconnect_event (uint16_t conn_handle)
{
    uint8_t buffer [4];

    buffer[0] = MTI_CHANNEL_SYSTEM;
    buffer[1] = MTI_SYSTEM_DISCONNECT;
    buffer[2] = LO_UINT16(conn_handle);
    buffer[3] = HI_UINT16(conn_handle);

    return  mti_queue_msg (buffer, sizeof(buffer));
}



/*************************************************
 * Function:    mti_send_indicate_confirm_event
 * Description: 发送接收到 indicate 对应的 confirm 事件
 * Input:       conn_handle - 连接的句柄
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_indicate_confirm_event (uint16_t conn_handle)
{
    uint8_t buffer [4];

    buffer[0] = MTI_CHANNEL_SYSTEM;
    buffer[1] = MTI_SYSTEM_CONFIRM;
    buffer[2] = LO_UINT16(conn_handle);
    buffer[3] = HI_UINT16(conn_handle);

    return  mti_queue_msg (buffer, sizeof(buffer));
}


/*************************************************
 * Function:    mti_send_request_data
 * Description: 发送接收到通道数据 事件
 * Input:       conn_handle - 连接句柄
 *              data_buf - 通道数据缓冲区
 *              length - 通道数据长度
 * Output:      无
 * Return:      MTI_SEND_SUCCESS - 事件发送成功
 *              MTI_SEND_BUFFER_FULL - 发送缓冲区已满
 *              MTI_SEND_INVALID_LENGTH - 数据长度非法
 * Others:      无
*************************************************/
MTI_SEND_STATUS_T    mti_send_request_data (uint16_t conn_handle,
                                                    const uint8_t *data_buf, 
                                                    uint8_t length)

{
    uint8_t buffer[MTI_BUFFER_DATA_SIZE];

    /*  检查数据长度      */
    if (length > MTI_BLE_DATA_SIZE)
    {
        return  MTI_SEND_INVALID_LENGTH;
    }

    /*  填充数据    */
    buffer[0] = MTI_CHANNEL_REQUEST;
    buffer[1] = LO_UINT16(conn_handle);
    buffer[2] = HI_UINT16(conn_handle);
    Osal_MemCpy (&buffer[3], data_buf, length);

    return  mti_queue_msg (buffer, length + 3);
}




/*************************************************
 * Function:    mti_receive_version_event
 * Description: 接收到版本信息时的处理函数
 * Input:       version_buf - 版本信息缓冲区
 *              length - 版本信息长度
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
*************************************************/
__weak MTI_PROCESS_STATUS_T    mti_receive_version_event (const uint8_t *version_buf, uint8_t length)
{
    return MTI_PROCESS_SUCCESS;
}


/*************************************************
 * Function:    mti_receive_connection_cnt_event
 * Description: 接收到最大连接数量的处理函数
 * Input:       connection_cnt - 最大连接数量
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
__weak MTI_PROCESS_STATUS_T    mti_receive_connection_cnt_event (uint8_t connection_cnt)
{
    return MTI_PROCESS_SUCCESS;
}

/*************************************************
 * Function:    mti_receive_device_info_event
 * Description: 接收到设置设备信息时的处理函数
 * Input:       id - 设备信息代码
 *              info_buf - 设备信息缓冲区
 *              length - 设备信息长度
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
__weak MTI_PROCESS_STATUS_T    mti_receive_device_info_event (uint8_t id, const uint8_t *info_buf, uint8_t length)
{
    return MTI_PROCESS_SUCCESS;
}



/*************************************************
 * Function:    mti_receive_device_info_event
 * Description: 接收到请求断开连接时的处理函数
 * Input:       conn_handle - 连接句柄
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
__weak MTI_PROCESS_STATUS_T    mti_receive_request_disconnect_event (uint16_t conn_handle)
{
    return MTI_PROCESS_SUCCESS;
}



/*************************************************
 * Function:    mti_receive_response_data_event
 * Description: 接收到通道数据时的处理函数
 * Input:       conn_handle - 连接句柄
 *              data_buf - 数据缓冲区
 *              length - 数据长度
 * Output:      无
 * Return:      处理结果状态
 * Others:      在 MTI.c 中只有 weak 的弱实现，应用程序应该在其他地方实现自己的处理
 *************************************************/
__weak MTI_PROCESS_STATUS_T    mti_receive_response_data_event (uint16_t conn_handle,
                                                    const uint8_t *data_buf, uint8_t length)

{
    return MTI_PROCESS_SUCCESS;
}



