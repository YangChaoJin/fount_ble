#ifndef _APPS_PROGRAM_H_
#define _APPS_PROGRAM_H_
#include <stdint.h>
#define CONNNET_EVENT               0x01



typedef enum 
{
	system_null=0,
    app_data_proc_event,
    dma_data_proc_event,
    null_event
}system_status;

typedef enum 
{
	spi_null=0,
    star_event,
    proc_event,
    end_event
}spi_status;


typedef struct 
{   
    uint8_t connet_num;
    uint8_t connet_flag;
}connet_event;

typedef struct 
{   
    volatile int busy; 
    uint8_t clock;
    system_status cache_event;
    system_status system_event;
}system_event;
typedef struct
{
    uint16_t size;
    uint8_t buffer[30];
}queue_msg;

uint8_t  get_connet_flag(uint8_t flag);
void set_connet_num_flag(uint8_t value);

void connet_event_flag_set(uint8_t flag);

void connet_event_flag_clear(uint8_t flag);

void system_event_set(system_status value);
void system_event_busy_set(void); 
void system_event_busy_clear(void);
uint8_t multi_connection(void);
uint8_t toggle_state(void);
#endif

