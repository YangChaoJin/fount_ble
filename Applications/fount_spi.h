#ifndef _FOUNT_SPI_H_
#define _FOUNT_SPI_H_
#include "BlueNRG1_conf.h"
#define SPI_CS_PIN            GPIO_Pin_11

#define SPI_OUT_MODE          Serial0_Mode
#define SPI_OUT_PIN           GPIO_Pin_2
#define SPI_IN_PIN            GPIO_Pin_3
#define SPI_IN_MODE           Serial0_Mode
#define SPI_CLK_PIN           GPIO_Pin_8
#define SPI_CLK_MODE          Serial0_Mode
#define SPI_BUFF_SIZE                40
#define SPI_TRANSFER_IDLE                1
#define SPI_TRANSFER_BUSY                0

typedef enum
{
    SPI_SEND_SUCCESS,
    SPI_SEND_BUSY,
    SPI_SEND_IDLY,
    SPI_INVALID_LENGTH,
}spi_status_t;

void spi_init(void);
uint8_t   get_spi_busy_status(void);
uint8_t spi_read_data( uint8_t* pBuffer, uint8_t NumByteToRead );
spi_status_t spi_send( uint8_t* pbuffer, uint8_t len);
uint8_t  busy_reset(void);
void spi_cs_control(uint8_t status);
uint8_t is_spi_busy(void);
#endif

