#include "ble_mutually.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fifo.h"
#include "osal.h"
#include "ble_status.h"
#include "BlueNRG1_conf.h"
#include "SDK_EVAL_Config.h"
#include "apps_program.h"
#include "bluenrg1_stack.h"
#include "gatt_uuid.h"
#include "connection_service.h"
#include "clock.h"

#define QUEUE_MAX_SIZE   80
#define QUEUE_ALIGNMENT   2

/**************************静态函数************************************/

static uint8_t  mti_response_version (uint8_t len, uint8_t *pData);
static uint8_t  mti_process_system_data (uint8_t length, uint8_t *pData);
static uint8_t  mti_response_max_conns (uint8_t num);
static uint8_t  mti_set_dev_Info(uint8_t id, uint8_t length, uint8_t *pData);
static uint8_t  mti_receive_data(uint8_t channel, uint16_t connHandle, uint8_t len, uint8_t *pData);
/**************************回调函数************************************/
static  MTICBs_t *pMTIAppCB = NULL;

/**************************静态变量************************************/
static  uint8_t  queue_buffer[40]={0};
// max num of available connections
static uint8_t maxAvailConns = MAX_NUM_BLE_CONNS;
static  circular_fifo_t s_ecomo_queue;
static const MTICBs_t simpleBLEMulti_MTICBs =
{
	mti_response_version,
	mti_response_max_conns,
	mti_set_dev_Info,
	mti_receive_data
};
/**************************全局变量************************************/
uint8_t g_dma_rv_flag = 0;

/*********************************************************************
 * @fn      MTI_registerAppCb
 *
 * @brief   注册回调函数
 * 			程序必须注册回调函数，然后才能开始发送数据
 *
 * @param   cb 回调函数结构体.
 *
 * @return  SUCCESS or FAILURE.
 */
uint8_t MTI_registerAppCb (MTICBs_t *cb)
{
	if (pMTIAppCB == NULL)
	{
		pMTIAppCB = cb;
		return SUCCESS;
	} else {
		return	FAILURE;
	}
}
 /***************************************************************************************
*	Function: void queue_init(void)
*	Description: 队列的申请
*	Input: 无
*	Output: 无
*	Return: 无
*	Others: 2018.11.30
*****************************************************************************************/
void queue_init(void)
{
      // 注册回调函数
    MTI_registerAppCb ((MTICBs_t*)&simpleBLEMulti_MTICBs);
    fifo_init(&s_ecomo_queue, QUEUE_MAX_SIZE, queue_buffer, QUEUE_ALIGNMENT);

}

/*********************************************************************
 * @fn      MTI_responseVersionCB
 *
 * @brief   Callback function when mcu send real version.
 *
 * @param   len - version length
 * @param	pData - version value
 *
 * @return  SUCCESS.
 */
static uint8_t  mti_response_version (uint8_t len, uint8_t *pData)
{

	ecomo_set_version (len, pData);

	return mti_request_max_conns ();
}

uint8_t mti_request_max_conns (void)
{
	uint8_t buffer[2];

	buffer[0] = MTI_CHANNEL_SYSTEM;
	buffer[1] = MTI_SYSTEM_MAX_CONNS;

	return	mti_queue_put (2, buffer);
}
 /***************************************************************************************
*	Function: mit_queue_rev(uint16_t *size, uint8_t  *buffer)
*	Description: 队列收到数据
*	Input:  size大小 buffer 数组的地址
*	Output: 
*	Return: 1 成功 0 失败
*	Others: 2018.11.30
*****************************************************************************************/
uint8_t mti_queue_rev(uint16_t *size, uint8_t  *buffer)
{
    return  fifo_get_var_len_item(&s_ecomo_queue, size, buffer);
}
 /***************************************************************************************
*	Function: uint8_t MTI_queuePut(uint16_t len,uint8_t *pdata)
*	Description: 队列发数据
*	Input:  size大小 buffer 数组的地址
*	Output: 
*	Return: 1 成功 0 失败
*	Others: 2018.11.30
*****************************************************************************************/
uint8_t mti_queue_put(uint16_t len,uint8_t *pdata)
{
    return fifo_put_var_len_item(&s_ecomo_queue, len, pdata); 
}

uint8_t MTI_connect (uint16_t connHandle)
{
	uint8_t	buffer [4];

	buffer[0] = MTI_CHANNEL_SYSTEM;
	buffer[1] = MTI_SYSTEM_CONNECT;
	buffer[2] = LO_UINT16(connHandle);
	buffer[3] = HI_UINT16(connHandle);

	return	mti_queue_put (4, buffer);
}


uint8_t MTI_sendData (uint8_t channel, uint16_t connHandle, uint8_t length, uint8_t *pData)
{
	uint8_t buffer [MTI_PDU_MAX_DATA];

	if (channel == MTI_CHANNEL_SYSTEM)
	{
		return	FAILURE;
	}

	buffer[0] = channel;
	buffer[1] = LO_UINT16(connHandle);
	buffer[2] = HI_UINT16(connHandle);
	memcpy (&buffer[3], pData, length);

	return	mti_queue_put (length+3, buffer);
}
 /***************************************************************************************
*	Function: mti_receive_data(uint8_t channel, uint16_t connHandle, uint8_t len, uint8_t *pData);
*	Description: 接受到单片机的数据处理
*	Input:  size大小 buffer 数组的地址
*	Output: 
*	Return: 1 成功 0 失败 BLE_STATUS_INSUFFICIENT_RESOURCES:协议栈堆栈溢出
*	Others: 2018.11.30
*****************************************************************************************/
static uint8_t  mti_receive_data(uint8_t channel, uint16_t connHandle, uint8_t len, uint8_t *pData)
{
    return gatt_update_notify_value (channel,connHandle,len,pData);
}
 /***************************************************************************************
*	Function: mti_set_dev_Info(uint8_t id, uint8_t length, uint8_t *pData);
*	Description: 队列发数据
*	Input:  size大小 buffer 数组的地址
*	Output: 
*	Return: 1 成功 0 失败 BLE_STATUS_INSUFFICIENT_RESOURCES:协议栈堆栈溢出
*	Others: 2018.11.30
*****************************************************************************************/
static uint8_t  mti_set_dev_Info(uint8_t id, uint8_t length, uint8_t *pData)
{
    return set_dev_info_parmeter(id,length,pData);
}
/*********************************************************************
 * @fn      MTI_responseMaxConnsCB
 *
 * @brief   Callback function when mcu response max number of available connections.
 *
 * @param	num - max num of available connections
 *
 * @return  SUCCESS.
 */
static uint8_t mti_response_max_conns (uint8_t num)
{
    uint8_t ret;
	if (num < MAX_NUM_BLE_CONNS)
	{
		maxAvailConns = num;
	}
/*******************开始广播********************************/
    ret = creat_connection();

	return	ret;
}

/*********************************************************************
 * @fn      MTI_transfer
 *
 * @brief   数据传输
 *
 * @param	length - 要发送的数据长度
 * @param	pSend - 要发送的数据
 * @param	pLen - 接收到的数据长度
 * @param	pRcv - 接收到的数据
 *
* @return  1 :SUCCESS or 0: FAILURE.
 */
uint8_t mti_transfer (uint8_t length, uint8_t *pSend)
{

    return SUCCESS;
}

/*********************************************************************
 * @fn      MTI_processRcvData
 *
 * @brief   分析处理收到的数据
 *
 * @param   cb 回调函数结构体.
 *
 * @return  SUCCESS or FAILURE.
 */
uint8_t mti_process_rcv_data (uint8_t length, uint8_t *pData)
{
	uint8_t status;
    uint16_t connHandle = 0;
	if (length == 0)
		return	FAILURE;

	if (pData[0] == MTI_CHANNEL_SYSTEM)
	{
		return	mti_process_system_data (length-1, &pData[1]);
	} 
    else 
    {
        if (length < 4)
        {
            return	FAILURE;
        }

         connHandle = BUILD_UINT16 (pData[1], pData[2]);

        pMTIAppCB->pfn_receiv_edata (pData[0], connHandle, length-3, &pData[3]);
        return status;
	}
}

/*********************************************************************
 * @fn      MTI_processSystemData
 *
 * @brief   分析处理收到的系统消息数据
 *
 * @param   cb 回调函数结构体.
 *
 * @return  SUCCESS or FAILURE.
 */
static uint8_t mti_process_system_data (uint8_t length, uint8_t *pData)
{
	uint8_t status;

	if (length == 0)
	{
		return	FAILURE;
	}
	switch (pData[0])
	{
	case	MTI_SYSTEM_VERSION:
		if (pMTIAppCB == NULL
			|| pMTIAppCB->pfn_response_version == NULL)
		{
			return	FAILURE;
		}
	  status = pMTIAppCB->pfn_response_version (length-1, &pData[1]);
		return	status;

	case	MTI_SYSTEM_MAX_CONNS:
		if (length != 2
			|| pMTIAppCB == NULL
			|| pMTIAppCB->pfn_response_max_conns == NULL)
		{
			return	FAILURE;
		}
		status = pMTIAppCB->pfn_response_max_conns(pData[1]);
		return	status;

	case	MTI_SYSTEM_DEVINFO:
		if (length < 2
            || pMTIAppCB == NULL
			|| pMTIAppCB->pfn_set_devInfo == NULL)
		{
			return	FAILURE;
		}
		status = pMTIAppCB->pfn_set_devInfo (pData[1], length-2, &pData[2]);
		return	status;

	default:
		return	FAILURE;
	}

}



















