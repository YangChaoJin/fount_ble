/*************************************************
Copyright (C), 2018-2018, Ecomo Tech. Co., Ltd.
File name:  transfer.h
Author: 王明浩        Version: V1.0       Date: 2018-12-17
Description:    负责与MSP430的数据交换，包括数据传输和重试
Others:
Function List:  // 主要函数列表，每条记录应包括函数名及功能简要说明
    1. transfer_init 初始化传输管理
    2. transfer_tick 状态机切换入口
    3. transfer_send 申请发送数据
    4. transfer_is_busy 检查传输是否完毕
    5. transfer_read 读取传输结果

History:
    1.  Version: V1.0
        Date: 2018-12-17
        Author: 王明浩
        Modification: 初次编码
*************************************************/


#ifndef     __TRANSFER_H__
#define     __TRANSFER_H__


#include <stdbool.h>
#include <stdint.h>




/*  transfer_send 时，允许的最大数据长度 */
#define TRANSFER_SEND_SIZE      (23)



/*  申请发送数据时的返回状态    */
typedef enum
{
    TRANSFER_SUCCESS,   /*  操作成功    */
    TRANSFER_BUSY,      /*  正处于忙碌状态，操作失败*/
    TRANSFER_INVALID_LENGTH,    /*  操作的长度*/
} TRANSFER_STATUS_T;





/*************************************************
 * Function:    transfer_init
 * Description: 初始化传输协议层
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    transfer_init (void);



/*************************************************
 * Function:    transfer_tick
 * Description: 状态机切换工作入口
 * Input:       无
 * Output:      无
 * Return:      无
 * Others:      无
*************************************************/
void    transfer_tick (void);


/*************************************************
 * Function:    transfer_send
 * Description: 申请发送数据
 * Input:       send_buf - 要发送的数据的缓冲区
 *              size - 要发送的数据长度
 * Output:      无
 * Return:      TRANSFER_STATUS_T -发送是否成功
 *              若为 TRANSFER_SUCCESS，代表发送成功
 *              若为 TRANSFER_BUSY，代表之前的传输尚未结束，应等待空闲后再次尝试发送
 *              若为 TRANSFER_INVALID_LENGTH，代表申请发送的数据长度过长，不用再次尝试
 * Others:      无
*************************************************/
TRANSFER_STATUS_T    transfer_send (const uint8_t *send_buf, uint16_t size);


/*************************************************
 * Function:    transfer_is_busy
 * Description: 检查传输是否处于忙碌状态
 * Input:       无
 * Output:      无
 * Return:      true 代表当前传输尚未结束
 *              false 代表传输结束，可以读取之前传输的结果，或启动新的传输
 * Others:      无
*************************************************/
bool    transfer_is_busy (void);


/*************************************************
 * Function:    transfer_read
 * Description: 读取传输结果
 * Input:       read_buf - 返回的传输结果缓冲区
 *              read_buf_size - 结果缓冲区长度
 * Output:      无
 * Return:      传输结果实际长度
 * Others:      应在传输完毕后读取传输结果，使用 tansfer_is_busy 判断传输是否结束；
 *              若在传输尚未结束时读取，则返回的传输实际长度为 0
*************************************************/
uint16_t transfer_read (uint8_t *read_buf, uint16_t read_buf_size);


#endif      /*  __TRANSFER_H__  */

